
package com.waar.app.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Error{
	@SerializedName("code")
   	private String code;
	@SerializedName("description")
   	private String description;
	@SerializedName("isClean")
   	private String isClean;

 	public String getCode(){
		return this.code;
	}
	public void setCode(String code){
		this.code = code;
	}
 	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = description;
	}
 	public String getIsClean(){
		return this.isClean;
	}
	public void setIsClean(String isClean){
		this.isClean = isClean;
	}
}
