
package com.waar.app.entity;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;
@Parcel(Parcel.Serialization.BEAN)
public class Data {
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private String id;
    @SerializedName("path")
    private String path;
    @SerializedName("video")
    private String video;
    @SerializedName("title")
    private String title;
    @SerializedName("type")
    private String type;

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
}
