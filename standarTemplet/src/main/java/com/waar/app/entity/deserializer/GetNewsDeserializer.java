package com.waar.app.entity.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.waar.app.entity.GetNews;

import java.lang.reflect.Type;

/**
 * Created by Alaa Al-Shaikh on 2016-12-29.
 */
public class GetNewsDeserializer implements JsonDeserializer<GetNews> {
    @Override
    public GetNews deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        GetNews response = gson.fromJson(json, GetNews.class);
        return response;
    }
}
