package com.waar.app.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetNews {
    @SerializedName("data")
    private List<Data> data = new ArrayList<Data>();// empty
    @SerializedName("error")
    private Error error;

    public List<Data> getData() {
        return this.data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public Error getError() {
        return this.error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
