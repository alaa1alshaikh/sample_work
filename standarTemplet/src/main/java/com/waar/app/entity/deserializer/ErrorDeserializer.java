package com.waar.app.entity.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.waar.app.entity.Error;

import java.lang.reflect.Type;

/**
 * Created by Alaa Al-Shaikh on 2016-12-30.
 */
public class ErrorDeserializer implements JsonDeserializer<Error> {
    @Override
    public Error deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Gson gson = new Gson();
        Error error = gson.fromJson(json, Error.class);
        return error;

    }
}
