package com.waar.app.permissions;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class DefaultPermissionProvider implements PermissionProvider {

    private final Activity mActivity;

    public DefaultPermissionProvider(@NonNull Activity activity) {
        mActivity = activity;
    }

    @Override
    public boolean hasPermission(String permission) {
        return ContextCompat.checkSelfPermission(mActivity, permission) ==
                PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public boolean shouldShowPermissionRequestRationale(String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission);
    }
}
