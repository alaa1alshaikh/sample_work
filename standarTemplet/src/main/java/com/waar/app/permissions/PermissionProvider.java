package com.waar.app.permissions;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public interface PermissionProvider {
    boolean hasPermission(String permission);

    boolean shouldShowPermissionRequestRationale(String permission);

}
