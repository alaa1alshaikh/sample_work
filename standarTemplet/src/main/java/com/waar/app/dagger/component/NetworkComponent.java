package com.waar.app.dagger.component;

import com.waar.app.dagger.annotation.LanguageScope;
import com.waar.app.dagger.module.AppModule;
import com.waar.app.dagger.module.NetworkModule;
import com.waar.app.settings.LanguageToken;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by Alaa Al-Shaikh on 2016-12-21.
 */

@Singleton
@LanguageScope
@Component(modules = {NetworkModule.class, AppModule.class})
public interface NetworkComponent {

    Retrofit retrofit();

    LanguageToken languageToken();
}
