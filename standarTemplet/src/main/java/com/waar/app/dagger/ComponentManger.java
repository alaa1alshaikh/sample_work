package com.waar.app.dagger;

import com.waar.app.settings.LanguageToken;

/**
 * Created by Alaa Al-Shaikh on 2016-12-21.
 */
public interface ComponentManger {

    LanguageToken getLanguageToken();
}
