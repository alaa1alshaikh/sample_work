package com.waar.app.dagger.module;

import com.waar.app.dagger.annotation.NewsApiScope;
import com.waar.app.dagger.annotation.NewsDetailsPresenterScope;
import com.waar.app.dagger.annotation.NewsMangerScope;
import com.waar.app.dagger.annotation.NewsPresenterScope;
import com.waar.app.fragment.news.NewsPresenter;
import com.waar.app.fragment.news_deatails.NewsDetailsPresenter;
import com.waar.app.network.news.NewsApi;
import com.waar.app.network.news.NewsManger;
import com.waar.app.settings.LanguageToken;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Alaa Al-Shaikh on 2016-12-20.
 */

@Module
public class NewsModule {

    @Provides
    @NewsApiScope
    NewsApi provideNewsApi(Retrofit retrofit) {
        return retrofit.create(NewsApi.class);
    }

    @Provides
    @NewsMangerScope
    NewsManger provideNewsManger(NewsApi newsApi) {
        return new NewsManger(newsApi);
    }

    @Provides
    @NewsPresenterScope
    NewsPresenter provideNewsPresenter(NewsApi newsApi, LanguageToken languageToken) {
        return new NewsPresenter(new NewsManger(newsApi), languageToken);
    }

    @Provides
    @NewsDetailsPresenterScope
    NewsDetailsPresenter provideNewsDetailsPresenter(NewsApi newsApi) {
        return new NewsDetailsPresenter(new NewsManger(newsApi));
    }

}
