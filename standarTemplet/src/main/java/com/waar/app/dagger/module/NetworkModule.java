package com.waar.app.dagger.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waar.app.BuildConfig;
import com.waar.app.dagger.annotation.LanguageScope;
import com.waar.app.entity.Data;
import com.waar.app.entity.Error;
import com.waar.app.entity.GetNews;
import com.waar.app.entity.deserializer.DataDeserializer;
import com.waar.app.entity.deserializer.ErrorDeserializer;
import com.waar.app.entity.deserializer.GetNewsDeserializer;
import com.waar.app.settings.LanguageToken;
import com.waar.app.utils.Config;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by Alaa Al-Shaikh on 2016-12-20.
 */

@Module
public class NetworkModule {
    private static final String STORE_NAME = "LanguageToken";

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(Application application) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(STORE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences;
    }


    @Provides
    @LanguageScope
    LanguageToken provideLanguageToken(SharedPreferences sharedPreferences) {
        return new LanguageToken(sharedPreferences);
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ?
                HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.NONE);
        return interceptor;

    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().registerTypeAdapter(GetNews.class, new GetNewsDeserializer())
                .registerTypeAdapter(Data.class, new DataDeserializer())
                .registerTypeAdapter(Error.class, new ErrorDeserializer()).create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        return client;

    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient client) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.NEWS_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                //.addConverterFactory(converterFactory)
                .build();
        return retrofit;

    }


}
