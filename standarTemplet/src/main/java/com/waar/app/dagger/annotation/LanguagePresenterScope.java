package com.waar.app.dagger.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Alaa Al-Shaikh on 2016-12-21.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface LanguagePresenterScope {
}
