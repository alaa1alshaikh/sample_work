package com.waar.app.dagger.component;

import com.waar.app.dagger.annotation.NewsApiScope;
import com.waar.app.dagger.annotation.NewsDetailsPresenterScope;
import com.waar.app.dagger.annotation.NewsMangerScope;
import com.waar.app.dagger.annotation.NewsPresenterScope;
import com.waar.app.dagger.module.NewsModule;
import com.waar.app.fragment.news.NewsPresenter;
import com.waar.app.fragment.news_deatails.NewsDetailsPresenter;

import dagger.Component;

/**
 * Created by Alaa Al-Shaikh on 2016-12-21.
 */
@Component(dependencies = NetworkComponent.class, modules = {NewsModule.class})
@NewsPresenterScope
@NewsApiScope
@NewsMangerScope
@NewsDetailsPresenterScope
public interface NewsComponent {

    NewsPresenter provideNewsPresenter();

    NewsDetailsPresenter provideNewsDetailsPresenter();
}
