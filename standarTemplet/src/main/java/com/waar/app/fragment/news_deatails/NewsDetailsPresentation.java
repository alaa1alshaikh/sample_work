package com.waar.app.fragment.news_deatails;

import android.graphics.Bitmap;

import com.waar.app.entity.Data;

/**
 * Created by Alaa Al-Shaikh on 2016-12-16.
 */
public interface NewsDetailsPresentation {
    void openVideo();

    void openZoomView();

    void fillData(Data data);

    void updateImage(Bitmap bitmap);
}
