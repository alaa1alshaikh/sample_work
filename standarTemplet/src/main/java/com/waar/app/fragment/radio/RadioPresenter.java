package com.waar.app.fragment.radio;

import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.view.View;

import com.waar.app.R;
import com.waar.app.fragment.BasePresenter;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class RadioPresenter implements BasePresenter<RadioPresentation>, View.OnClickListener,
        MediaPlayer.OnBufferingUpdateListener , MediaPlayer.OnCompletionListener {

    private RadioPresentation radioPresentation;

    @Override
    public void onCreateView(@NonNull RadioPresentation presentation) {
        radioPresentation = presentation;
    }

    @Override
    public void onDestroyView() {
        radioPresentation = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_play:
                radioPresentation.playRadio();
                break;
            case R.id.btn_pause:
                radioPresentation.pauseRadio();
                break;
            case R.id.btn_stop:
                radioPresentation.stopRadio();
                break;
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        radioPresentation.updateSeekBar(percent);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        radioPresentation.completeRadio();
    }


}
