package com.waar.app.fragment.news_deatails;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.crystal.crystalpreloaders.widgets.CrystalPreloader;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.waar.app.App;
import com.waar.app.R;
import com.waar.app.entity.Data;
import com.waar.app.fragment.BaseFragment;
import com.waar.app.thirdParty.views.CustomScrollView;
import com.waar.app.thirdParty.views.CustomTextView;
import com.waar.app.utils.Config;
import com.waar.app.utils.Global;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class NewsDetailsFragment extends BaseFragment implements NewsDetailsPresentation, OnClickListener {
    public static final String TAG = NewsDetailsFragment.class.getSimpleName();
    public static final int SHARE_ID = 0123;

    @BindView(R.id.pagerLayout)
    RelativeLayout imageLayout;

    @BindView(R.id.image)
    ImageView imageImage;

    @BindView(R.id.imageVideo)
    ImageView videoImage;

    @BindView(R.id.loader_news_details)
    CrystalPreloader preloader;

    @BindView(R.id.advtitle)
    CustomTextView title;

    @BindView(R.id.advSummery)
    CustomTextView sSummery;


    @BindView(R.id.scrollView)
    CustomScrollView scrollView;


    private String id;
    private String actionTitle;
    private Data article;
    private NewsDetailsPresenter newsDetailsPresenter;
    private ImageLoader imageLoader;
    private int width;
    private View rootView;

    private void initImageLoader() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
    }

    public static NewsDetailsFragment newInstance(Data article) {
        NewsDetailsFragment fragment = new NewsDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(Config.ARTICLE_DETAILS, Parcels.wrap(article));
        fragment.setArguments(args);
        return fragment;
    }

    public static NewsDetailsFragment newInstance(String id, String title) {
        NewsDetailsFragment fragment = new NewsDetailsFragment();
        Bundle args = new Bundle();
        args.putString(Config.ARTICLE_DETAILS_ID, id);
        args.putString(Config.ARTICLE_DETAILS_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  article = Parcels.unwrap(getArguments().getParcelable(Config.ARTICLE_DETAILS));
        id = getArguments().getString(Config.ARTICLE_DETAILS_ID);
        actionTitle = getArguments().getString(Config.ARTICLE_DETAILS_TITLE);
        newsDetailsPresenter = ((App) getActivity().getApplication()).getNewsComponent().provideNewsDetailsPresenter();
        newsDetailsPresenter.onCreateView(this);
        initImageLoader();

    }

    @Override
    protected boolean shouldLockDrawer() {
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add(0, SHARE_ID, 0, R.string.Share).setIcon(R.drawable.ic_share)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        getDrawerManager().showUpButton();
        getDrawerManager().setToolbarTitle(actionTitle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = (View) inflater
                    .inflate(R.layout.fragment_news_details, null, false);
            ButterKnife.bind(this, rootView);
            imageImage.setOnClickListener(this);
            newsDetailsPresenter.fetchNewsFromId(id);
            Display display;
            display = getActivity().getWindowManager().getDefaultDisplay();
            width = display.getWidth();
        }
        return rootView;
    }


    @Override
    public void openVideo() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(article.getVideo()));
        startActivity(intent);
    }

    @Override
    public void openZoomView() {
        getNavigationManager().navigateToZoomImageScreen(article.getPath());
    }

    @Override
    public void fillData(Data data) {
        scrollView.setVisibility(View.VISIBLE);
        preloader.setVisibility(View.INVISIBLE);
        article = data;
        if (article.getPath().length() == 0) {
            imageLayout.setVisibility(View.GONE);
        }
        if (article.getType().equalsIgnoreCase(Config.VIDEO)) {
            videoImage.setVisibility(View.VISIBLE);
            newsDetailsPresenter.setVideo(true);
        } else if (article.getType().equalsIgnoreCase(Config.IMAGE)) {

            newsDetailsPresenter.setVideo(false);
            Picasso.with(getActivity()).load(article.getPath()).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    updateImage(bitmap);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
//            imageLoader.displayImage(article.getPath(), imageImage,
//                    null, newsDetailsPresenter);
        }


        title.setText(article.getTitle());
        sSummery.setText(article.getDescription());
    }

    @Override
    public void updateImage(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.getHeight();
            bitmap.getWidth();

            int newHeight = width
                    * bitmap.getHeight()
                    / bitmap.getWidth();

            imageImage.getLayoutParams().height = newHeight;
            imageImage.setImageBitmap(bitmap);
            imageImage.requestLayout();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case SHARE_ID:
                Global.invitePeople(getActivity(), article);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image:
                newsDetailsPresenter.onClickPerform();
        }
    }
}
