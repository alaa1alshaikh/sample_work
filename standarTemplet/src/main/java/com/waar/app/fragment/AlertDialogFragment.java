package com.waar.app.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.waar.app.R;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class AlertDialogFragment extends DialogFragment {
    public static final String TAG = AlertDialogFragment.class.getSimpleName();

    private static final String MESSAGE = "message";

    private String mMessage;

    public static AlertDialogFragment newInstance(@NonNull String message) {
        Bundle args = new Bundle();
        args.putString(MESSAGE, message);
        AlertDialogFragment fragment = new AlertDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMessage = getArguments().getString(MESSAGE);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme)
                .setMessage(mMessage)
                .setNegativeButton(R.string.OK, null)
                .create();
    }
}
