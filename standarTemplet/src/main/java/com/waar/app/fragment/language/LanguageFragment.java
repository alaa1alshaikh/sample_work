package com.waar.app.fragment.language;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.waar.app.App;
import com.waar.app.R;
import com.waar.app.activity.MainActivity;
import com.waar.app.fragment.BaseFragment;
import com.waar.app.settings.LanguageToken;
import com.waar.app.utils.Config;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class LanguageFragment extends BaseFragment implements LanguagePresentation {

    public static final String TAG = LanguageFragment.class.getSimpleName();

    @BindView(R.id.english_lay)
    RelativeLayout englishLayout;

    @BindView(R.id.arabic_lay)
    RelativeLayout arabicLayout;

    @BindView(R.id.turkish_lay)
    RelativeLayout turkishLayout;

    @BindView(R.id.kordi_lay)
    RelativeLayout kurdishLayout;

    LanguagePresenter languagePresenter;

    @Inject
    LanguageToken languageToken;

    public static LanguageFragment newInstance() {
        return new LanguageFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languageToken = ((App) getActivity().getApplication()).getNetworkComponent().languageToken();
        languagePresenter = new LanguagePresenter(languageToken);
        languagePresenter.onCreateView(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_language, container, false);
        ButterKnife.bind(this, view);
        if (languageToken.getLocalLanguage().equalsIgnoreCase(Config.ARABIC)) {
            view.findViewById(R.id.arabic_lay_icon).setBackgroundResource(
                    R.drawable.ic_check_icon);
        } else if (languageToken.getLocalLanguage().equalsIgnoreCase(Config.ENGLISH)) {
            view.findViewById(R.id.english_lay_icon).setBackgroundResource(
                    R.drawable.ic_check_icon);
        } else if (languageToken.getLocalLanguage().equalsIgnoreCase(Config.TURKISH)) {// turkish
            view.findViewById(R.id.turkish_lay_icon).setBackgroundResource(
                    R.drawable.ic_check_icon);
        } else if (languageToken.getLocalLanguage().equalsIgnoreCase(Config.KURDISH)) {// kurdishan
            view.findViewById(R.id.kordi_lay_icon).setBackgroundResource(
                    R.drawable.ic_check_icon);
        }
        englishLayout.setOnClickListener(languagePresenter);
        arabicLayout.setOnClickListener(languagePresenter);
        turkishLayout.setOnClickListener(languagePresenter);
        kurdishLayout.setOnClickListener(languagePresenter);
        return view;
    }

    @Override
    public void reCreateActivity() {
        Locale locale = new Locale(languageToken.getLocalLanguage());
        Locale.setDefault(locale);
        Resources resources = getActivity().getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        Intent intent = new Intent(getActivity(),
                MainActivity.class);
        intent.putExtra(Config.LANGUAGE, true);
        startActivity(intent);
        getActivity().finish();
    }
}
