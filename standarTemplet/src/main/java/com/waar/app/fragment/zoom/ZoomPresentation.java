package com.waar.app.fragment.zoom;

import android.view.View;

import java.util.List;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public interface ZoomPresentation {
    void loadImages(List<String> images);
}
