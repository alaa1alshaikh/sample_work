package com.waar.app.fragment.news;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crystal.crystalpreloaders.widgets.CrystalPreloader;
import com.waar.app.App;
import com.waar.app.R;
import com.waar.app.adapter.news_adapter.NewsAdapter;
import com.waar.app.entity.Data;
import com.waar.app.events.NewsClickEvent;
import com.waar.app.fragment.BaseFragment;
import com.waar.app.settings.LanguageToken;
import com.waar.app.thirdParty.EndlessRecyclerViewScrollListener;
import com.waar.app.utils.Config;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import retrofit2.Retrofit;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class NewsFragment extends BaseFragment implements NewsPresentation {

    public static final String TAG = NewsFragment.class.getSimpleName();

    @BindView(R.id.news_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.news_list)
    RecyclerView newsList;

    @BindView(R.id.loader)
    CrystalPreloader preloader;

    private long type = 0;
    private NewsPresenter presenter;
    private NewsAdapter adapter;
    private View rootView;

    @Inject
    Retrofit mRetrofit;

    @Inject
    LanguageToken languageToken;

    public static NewsFragment newInstance(long type) {
        NewsFragment fragment = new NewsFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Config.TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getDrawerManager().setToolbarTitle(R.string.app_name);
        getDrawerManager().showBurgerButton();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        type = getArguments().getLong(Config.TYPE, 0);
        presenter = ((App) getActivity().getApplication()).getNewsComponent().provideNewsPresenter();
        presenter.onCreateView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_news_list, container, false);
            ButterKnife.bind(this, rootView);
            presenter.fetchNewsListFromId(type);
            swipeRefreshLayout.setOnRefreshListener(presenter);
        }
        return rootView;
    }

    @Override
    public void fillAdapter(List<Data> dataArrayList) {
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        preloader.setVisibility(View.INVISIBLE);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        newsList.addOnScrollListener(new EndlessRecyclerViewScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.loadMoreData();
            }
        });

        newsList.setLayoutManager(llm);
        adapter = new NewsAdapter(dataArrayList);
        newsList.setAdapter(adapter);

    }

    @Override
    public void goToNewsDetails(Data data) {
        getNavigationManager().navigateToArticleScreen(data);
    }

    @Override
    public void addNewDataToAdapter(List<Data> dataArrayList) {
        adapter.addAll(dataArrayList);
    }

    @Override
    public void updateRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    public void onEvent(NewsClickEvent event) {
        getNavigationManager().navigateToArticleScreen(event.getId(), event.getTitle());
    }
}
