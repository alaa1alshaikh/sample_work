package com.waar.app.fragment;

import android.support.annotation.NonNull;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public interface BasePresenter<T> {
    void onCreateView(@NonNull T presentation);

    void onDestroyView();

}
