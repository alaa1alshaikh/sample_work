package com.waar.app.fragment.video;

import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;

import com.waar.app.R;
import com.waar.app.fragment.BasePresenter;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.Header;

import java.io.IOException;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class VideoPresenter implements BasePresenter<VideoPresentation>, View.OnTouchListener, View.OnClickListener {

    private VideoPresentation videoPresentation;

    @Override
    public void onCreateView(@NonNull VideoPresentation presentation) {
        videoPresentation = presentation;
    }

    public void resolveRedirect(String url)
    {
        Header loc[] = new Header[0];
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpClientParams.setRedirecting(httpParameters, false);

            HttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(url);
            HttpContext context = new BasicHttpContext();

            HttpResponse response = null;

            response = httpClient.execute(httpget, context);

            // If we didn't get a '302 Found' we aren't being redirected.
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_MOVED_TEMPORARILY)
                throw new IOException(response.getStatusLine().toString());
            loc = response.getHeaders("Location");
        } catch (IOException e) {
            e.printStackTrace();
        }

        videoPresentation.playVideo(loc.length > 0 ? loc[0].getValue() : null);
    }

    @Override
    public void onDestroyView() {
        videoPresentation = null;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        videoPresentation.toggleController(v);
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play:
                videoPresentation.setPlayRadio();
                break;
            case R.id.pause:
                videoPresentation.pauseVideo();
                break;
            case R.id.stop:
                videoPresentation.stopVideo();
                break;
        }
    }
}
