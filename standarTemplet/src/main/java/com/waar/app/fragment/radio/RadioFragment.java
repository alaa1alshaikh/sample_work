package com.waar.app.fragment.radio;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.waar.app.R;
import com.waar.app.fragment.BaseFragment;
import com.waar.app.utils.Config;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class RadioFragment extends BaseFragment implements RadioPresentation {

    public static final String TAG = RadioFragment.class.getSimpleName();

    @BindView(R.id.btn_play)
    ImageButton mPlay;

    @BindView(R.id.btn_pause)
    ImageButton mPause;

    @BindView(R.id.btn_stop)
    ImageButton mStop;

    @BindView(R.id.seekBar)
    SeekBar seekBar;

    private MediaPlayer mediaPlayer;
    private String mCurrent;

    private int lengthOfAudio = 1000000;

    private final Handler updateSeekProgressHandler = new Handler();
    private final Runnable updateSeekProgressRunnable = new Runnable() {
        @Override
        public void run() {
            updateSeekProgress();
        }
    };

    private RadioPresenter radioPresenter;

    public static RadioFragment newInstance() {
        return new RadioFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        radioPresenter = new RadioPresenter();
        radioPresenter.onCreateView(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_radio, null, false);
        ButterKnife.bind(this, mView);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(radioPresenter);
        mediaPlayer.setOnCompletionListener(radioPresenter);
        mPlay.setOnClickListener(radioPresenter);
        mPause.setOnClickListener(radioPresenter);
        mStop.setOnClickListener(radioPresenter);
        return mView;
    }

    @Override
    public void onPause() {
        super.onPause();
        pauseRadio();
    }

    @Override
    public void playRadio() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(Config.CHANNEL_AUDIO_STRAMING);
        } catch (IllegalArgumentException e) {
        } catch (SecurityException e) {
        } catch (IllegalStateException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mediaPlayer.prepare();
        } catch (IllegalStateException e) {
        } catch (IOException e) {
        }
        mediaPlayer.start();
        updateSeekProgressHandler.post(updateSeekProgressRunnable);
        mPlay.setEnabled(false);
        mPause.setEnabled(true);
        mPause.setEnabled(true);
    }

    @Override
    public void pauseRadio() {
        mediaPlayer.pause();
        updateSeekProgressHandler.removeCallbacks(updateSeekProgressRunnable);
        mPlay.setEnabled(true);
        mPause.setEnabled(false);
        mStop.setEnabled(false);
    }

    @Override
    public void stopRadio() {
        mediaPlayer.stop();
        mPlay.setEnabled(true);
        mPause.setEnabled(false);
        mStop.setEnabled(false);
        seekBar.setProgress(0);
        updateSeekProgressHandler.removeCallbacks(updateSeekProgressRunnable);
    }

    @Override
    public void updateSeekBar(int percent) {
        seekBar.setSecondaryProgress(percent);
    }

    @Override
    public void completeRadio() {
        mPlay.setEnabled(true);
        mPause.setEnabled(false);
        mStop.setEnabled(false);
    }

    private void updateSeekProgress() {
        if (mediaPlayer.isPlaying()) {
            seekBar.setProgress((int) (((float) mediaPlayer
                    .getCurrentPosition() / lengthOfAudio) * 10));
            updateSeekProgressHandler.postDelayed(updateSeekProgressRunnable, 100);

        }
    }

}
