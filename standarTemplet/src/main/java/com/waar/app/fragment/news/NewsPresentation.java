package com.waar.app.fragment.news;

import com.waar.app.entity.Data;
import com.waar.app.fragment.BasePresenter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public interface NewsPresentation {
    void fillAdapter(List<Data> dataArrayList);

    void goToNewsDetails(Data data);

    void addNewDataToAdapter(List<Data> dataArrayList);

    void updateRefresh();
}
