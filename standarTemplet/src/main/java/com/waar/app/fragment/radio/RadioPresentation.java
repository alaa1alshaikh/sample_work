package com.waar.app.fragment.radio;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public interface RadioPresentation {
     void playRadio();

    void pauseRadio();

    void stopRadio();
    
    void updateSeekBar(int percent);

    void completeRadio();

}
