package com.waar.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import com.waar.app.activity.navigation.DrawerManager;
import com.waar.app.activity.navigation.DrawerManagerProvider;
import com.waar.app.activity.navigation.NavigationManager;
import com.waar.app.activity.navigation.NavigationManagerProvider;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class BaseFragment extends Fragment {
    private DrawerManager mDrawerManager;
    private NavigationManager mNavigationManager;
    private ActionBar mActionBar;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mDrawerManager = ((DrawerManagerProvider) getActivity()).getDrawerManager();
            mNavigationManager = ((NavigationManagerProvider) getActivity()).getNavigationManager();
            mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        } catch (ClassCastException e) {
            throw new IllegalStateException(getActivity().getClass().getSimpleName() + " must implement "
                    + NavigationManagerProvider.class.getSimpleName());
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getDrawerManager().lockDrawer(shouldLockDrawer());
        if (shouldShowActionBar()) {
            mActionBar.show();
        } else {
            mActionBar.hide();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mDrawerManager = null;
        mNavigationManager = null;
    }

    protected NavigationManager getNavigationManager() {
        return mNavigationManager;
    }

    protected DrawerManager getDrawerManager() {
        return mDrawerManager;
    }

    /**
     * By default the drawer will remain unlocked. Override to lock the drawer.
     *
     * @return True if the drawer should be locked when this fragment is displayed.
     */
    protected boolean shouldLockDrawer() {
        return false;
    }

    /**
     * By default the action bar will be displayed. Override to hide the action bar.
     *
     * @return True if the action bar should be displayed when this fragment is displayed.
     */
    protected boolean shouldShowActionBar() {
        return true;
    }

    /**
     * Shows a simple {@link android.support.v7.app.AlertDialog} with a message and a button to
     * dismiss the dialog.
     *
     * @param message The message to display
     */
    protected void showAlertDialog(@NonNull String message) {
        AlertDialogFragment fragment = AlertDialogFragment.newInstance(message);
        fragment.show(getFragmentManager(), AlertDialogFragment.TAG);
    }


}
