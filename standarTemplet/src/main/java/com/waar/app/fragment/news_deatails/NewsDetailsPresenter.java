package com.waar.app.fragment.news_deatails;

import android.support.annotation.NonNull;

import com.waar.app.entity.GetNews;
import com.waar.app.fragment.BasePresenter;
import com.waar.app.network.NetworkCallback;
import com.waar.app.network.NetworkResponse;
import com.waar.app.network.news.NewsManger;

import javax.inject.Inject;

/**
 * Created by Alaa Al-Shaikh on 2016-12-16.
 */
public class NewsDetailsPresenter implements BasePresenter<NewsDetailsPresentation> {


    private NewsDetailsPresentation newsDetailsPresentation;

    private NewsManger newsManger;

    private boolean isVideo;

    public void setVideo(boolean video) {
        isVideo = video;
    }

    @Inject
    public NewsDetailsPresenter(NewsManger newsManger) {
        this.newsManger = newsManger;
    }

    @Override
    public void onCreateView(@NonNull NewsDetailsPresentation presentation) {
        newsDetailsPresentation = presentation;
    }

    public void fetchNewsFromId(String id) {
        newsManger.getNewsDetails(new NetworkCallback<GetNews>() {
            @Override
            public void onResponse(@NonNull NetworkResponse<GetNews> response) {
                if (response.getBody().getData().size() > 0) {
                    newsDetailsPresentation.fillData(response.getBody().getData().get(0));
                }
            }

            @Override
            public void onFailure(@NonNull Throwable t) {
            }
        }, id);
    }

    private int width;

    @Override
    public void onDestroyView() {
        newsDetailsPresentation = null;
    }


    public void setWidth(int width) {
        this.width = width;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public void onClickPerform() {
        if (!isVideo)
            newsDetailsPresentation.openZoomView();
        else
            newsDetailsPresentation.openVideo();
    }
}
