package com.waar.app.fragment.zoom;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waar.app.R;
import com.waar.app.adapter.slideshow.ViewPhotoAdapter;
import com.waar.app.fragment.BaseFragment;
import com.waar.app.utils.Config;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaa Al-Shaikh on 2016-12-16.
 */
public class ZoomFragment extends BaseFragment implements ZoomPresentation {

    public static final String TAG = ZoomFragment.class.getSimpleName();

    private String imageUrl;
    private ZoomPresenter zoomPresenter;
    private ViewPhotoAdapter viewPhotoAdapter;

    @BindView(R.id.photo_pager)
    ViewPager viewPager;

    public static ZoomFragment newInstance(String image) {
        ZoomFragment fragment = new ZoomFragment();
        Bundle args = new Bundle();
        args.putString(Config.IMAGE_URL, image);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageUrl = getArguments().getString(Config.IMAGE_URL);
        zoomPresenter = new ZoomPresenter();
        zoomPresenter.onCreateView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = (View) inflater
                .inflate(R.layout.fragment_slideshow, null, false);
        ButterKnife.bind(this, mView);
        zoomPresenter.load(imageUrl);
        return mView;
    }

    @Override
    protected boolean shouldLockDrawer() {
        return true;
    }

    @Override
    public void loadImages(List<String> images) {
        viewPhotoAdapter = new ViewPhotoAdapter(images);
        viewPager.setAdapter(viewPhotoAdapter);
    }

}
