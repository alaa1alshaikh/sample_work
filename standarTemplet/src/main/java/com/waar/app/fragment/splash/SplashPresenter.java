package com.waar.app.fragment.splash;

import android.Manifest;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.waar.app.fragment.BasePresenter;
import com.waar.app.permissions.PermissionProvider;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class SplashPresenter implements BasePresenter<SplashPresentation> {

    private SplashPresentation splashPresentation;
    private PermissionProvider mPermissionProvider;

    public void setPermissionProvider(PermissionProvider mPermissionProvider) {
        this.mPermissionProvider = mPermissionProvider;
    }

    @Override
    public void onCreateView(@NonNull SplashPresentation presentation) {
        splashPresentation = presentation;
    }

    @Override
    public void onDestroyView() {
        splashPresentation = null;
    }


    public void splashScreen() {
        Handler handler = new Handler();
        Runnable action = new Runnable() {
            @Override
            public void run() {
                splashPresentation.dismissSplash();
            }
        };
        handler.postDelayed(action, 2000);

    }

    public void checkForStorageReadPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            splashScreen();
            return;
        }

        if (mPermissionProvider.hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            splashScreen();
            return;
        }

        if (mPermissionProvider.shouldShowPermissionRequestRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) && mPermissionProvider.shouldShowPermissionRequestRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            splashPresentation.showReadAndWriteExternalPermissionRationaleDialog();
        } else {
            requestReadExternalStoragePermission();
        }

    }

    public void requestReadExternalStoragePermission() {
        splashPresentation.showReadAndWriteExternalPermissionRequestDialog();
    }


}

