package com.waar.app.fragment.video;

import android.view.View;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public interface VideoPresentation {
    void toggleController(View v);

    void playVideo(String path);

    void stopVideo();

    void pauseVideo();

    void updateSeekProgress();

    void setPlayRadio();
}
