package com.waar.app.fragment.news;

import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;

import com.waar.app.entity.GetNews;
import com.waar.app.fragment.BasePresenter;
import com.waar.app.network.NetworkCallback;
import com.waar.app.network.NetworkResponse;
import com.waar.app.network.news.NewsManger;
import com.waar.app.settings.LanguageToken;

import javax.inject.Inject;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class NewsPresenter implements BasePresenter<NewsPresentation>, SwipeRefreshLayout.OnRefreshListener {
    private NewsPresentation presentation;
    private NewsManger newsManger;
    private LanguageToken languageToken;

    private String lastId;
    private long id;


    @Inject
    public NewsPresenter(NewsManger newsManger, LanguageToken languageToken) {
        this.newsManger = newsManger;
        this.languageToken = languageToken;
    }

    @Override
    public void onCreateView(@NonNull NewsPresentation presentation) {
        this.presentation = presentation;
    }

    @Override
    public void onDestroyView() {
        presentation = null;
    }

    public void fetchNewsListFromId(long id) {
        this.id = id;
        newsManger.getNews(new NetworkCallback<GetNews>() {
            @Override
            public void onResponse(@NonNull NetworkResponse<GetNews> response) {
                if (response.getBody().getData().size() > 0) {
                    lastId = String.valueOf(response.getBody().getData().get(response.getBody().getData().size() - 1).getId());
                }
                presentation.fillAdapter(response.getBody().getData());
                presentation.updateRefresh();
            }

            @Override
            public void onFailure(@NonNull Throwable t) {
                presentation.updateRefresh();
            }
        }, "0", languageToken.getServerLanguage(), id);
    }


    @Override
    public void onRefresh() {
        fetchNewsListFromId(id);
    }

    public void loadMoreData() {
        newsManger.getNews(new NetworkCallback<GetNews>() {
            @Override
            public void onResponse(@NonNull NetworkResponse<GetNews> response) {
                if (response.getBody().getData().size() > 0) {
                    lastId = String.valueOf(response.getBody().getData().get(response.getBody().getData().size() - 1).getId());
                }
                presentation.addNewDataToAdapter(response.getBody().getData());
            }

            @Override
            public void onFailure(@NonNull Throwable t) {
            }
        }, lastId, languageToken.getServerLanguage(), id);
    }
}
