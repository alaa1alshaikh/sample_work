package com.waar.app.fragment.splash;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public interface SplashPresentation {
    void dismissSplash();

    void showReadAndWriteExternalPermissionRequestDialog();

    void showReadAndWriteExternalPermissionRationaleDialog();
}
