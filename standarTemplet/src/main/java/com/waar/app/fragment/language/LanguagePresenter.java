package com.waar.app.fragment.language;

import android.support.annotation.NonNull;
import android.view.View;

import com.waar.app.R;
import com.waar.app.fragment.BasePresenter;
import com.waar.app.settings.LanguageToken;
import com.waar.app.utils.Config;

import javax.inject.Inject;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class LanguagePresenter implements
        View.OnClickListener, BasePresenter<LanguagePresentation> {

    private LanguagePresentation presentation;
    private LanguageToken languageToken;

    @Inject
    public LanguagePresenter(LanguageToken languageToken) {
        this.languageToken = languageToken;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.arabic_lay:
                languageToken.setLocalLanguage(Config.ARABIC);
                languageToken.setServerLanguage(Config.ARABIC);
                presentation.reCreateActivity();
                break;
            case R.id.english_lay:
                languageToken.setLocalLanguage(Config.ENGLISH);
                languageToken.setServerLanguage(Config.ENGLISH);
                presentation.reCreateActivity();
                break;
            case R.id.turkish_lay:
                languageToken.setLocalLanguage(Config.TURKISH);
                languageToken.setServerLanguage(Config.TURKISH_CKB);
                presentation.reCreateActivity();
                break;
            case R.id.kordi_lay:
                languageToken.setLocalLanguage(Config.KURDISH);
                languageToken.setServerLanguage(Config.KURDISH_CKB);
                presentation.reCreateActivity();
                break;

        }
    }


    @Override
    public void onCreateView(@NonNull LanguagePresentation presentation) {
        this.presentation = presentation;

    }

    @Override
    public void onDestroyView() {
        presentation = null;
    }
}
