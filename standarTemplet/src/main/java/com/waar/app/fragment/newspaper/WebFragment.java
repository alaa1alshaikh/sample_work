package com.waar.app.fragment.newspaper;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.waar.app.R;
import com.waar.app.fragment.BaseFragment;
import com.waar.app.utils.Config;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class WebFragment extends BaseFragment implements WebPresentation {
    public static final String TAG = WebFragment.class.getSimpleName();

    private static final String KEY_TITLE = "title";
    private static final String KEY_ARTICLE = "KEY_ARTICLE";


    private String title;
    private String url;

    private WebPresenter mPresenter;


    public static WebFragment newInstance(String title, String url) {
        WebFragment fragment = new WebFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Config.TITLE, title);
        bundle.putString(Config.URL, url);
        fragment.setArguments(bundle);
        return fragment;
    }


    public static WebFragment newInstance() {
        WebFragment fragment = new WebFragment();
        return fragment;
    }

    @BindView(R.id.article_webview)
    WebView webView;

    @BindView(R.id.progressBarLoading)
    ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(Config.TITLE)) {
            title = getArguments().getString(Config.TITLE);
            url = getArguments().getString(Config.URL);
        }

        mPresenter = new WebPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_webview, container, false);
        ButterKnife.bind(this, view);
        setupWebView();
        mPresenter.onCreateView(this);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getDrawerManager().setToolbarTitle(title);
     //   getDrawerManager().showUpButton();
    }

    @Override
    public void loadPage() {
        webView.loadUrl(url);

    }

    @Override
    public void toggleProgressBar(boolean show) {
        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        showAlertDialog(message);
    }


    private void setupWebView(){
        WebSettings webSettings = webView.getSettings();
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setJavaScriptEnabled(true);
        webView.clearHistory();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setVerticalScrollBarEnabled(true);
        webView.setHorizontalScrollBarEnabled(true);
        webView.setWebViewClient(mPresenter);
    }

}