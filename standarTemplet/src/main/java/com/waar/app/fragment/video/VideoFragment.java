package com.waar.app.fragment.video;

import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.VideoView;

import com.waar.app.R;
import com.waar.app.fragment.BaseFragment;
import com.waar.app.utils.Config;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class VideoFragment extends BaseFragment implements VideoPresentation {

    public static final String TAG = VideoFragment.class.getSimpleName();

    @BindView(R.id.surface_view)
    VideoView mVideoView;

    @BindView(R.id.play)
    ImageButton mPlay;

    @BindView(R.id.pause)
    ImageButton mPause;

    @BindView(R.id.stop)
    ImageButton mStop;

    @BindView(R.id.seekBar)
    SeekBar seekBar;

    @BindView(R.id.play_lay)
    LinearLayout linearLayout;

    private VideoPresenter videoPresenter;
    private int lengthOfAudio = 1000000;

    private final Runnable updateSeekBarRunnable = new Runnable() {
        @Override
        public void run() {
            updateSeekProgress();
        }
    };


    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoPresenter = new VideoPresenter();
        videoPresenter.onCreateView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_video, container, false);
        ButterKnife.bind(this, mView);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        linearLayout.setOnTouchListener(videoPresenter);
        mPlay.setOnClickListener(videoPresenter);
        mStop.setOnClickListener(videoPresenter);
        mPause.setOnClickListener(videoPresenter);
        return mView;
    }

    @Override
    public void toggleController(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(
                    View.INVISIBLE);
        } else {
            view.setVisibility(
                    View.VISIBLE);
        }
    }

    @Override
    public void playVideo(String path) {
        try {
            if (mVideoView != null && path != null) {
                Uri vidUri = Uri.parse(path);
                mVideoView.setVideoURI(vidUri);
                mVideoView.start();
                mVideoView.requestFocus();
                return;
            }

        } catch (Exception e) {
            Log.e("TAG", "error: " + e.getMessage(), e);
            if (mVideoView != null) {
                mVideoView.stopPlayback();
            }
        }
    }

    @Override
    public void stopVideo() {
        if (mVideoView != null) {
            mVideoView.stopPlayback();
            mPause.setEnabled(false);
            mStop.setEnabled(false);
            mPlay.setEnabled(true);
        }
    }

    @Override
    public void pauseVideo() {
        if (mVideoView != null) {
            mVideoView.pause();
            mPause.setEnabled(false);
            mStop.setEnabled(false);
            mPlay.setEnabled(true);
        }
    }

    private final Handler updateSeekBarHandler = new Handler();

    @Override
    public void updateSeekProgress() {
        if (mVideoView.isPlaying()) {
            seekBar.setProgress((int) (((float) mVideoView.getCurrentPosition() / lengthOfAudio) * 10));
            updateSeekBarHandler.postDelayed(updateSeekBarRunnable, 100);
        }
    }

    @Override
    public void setPlayRadio() {
        videoPresenter.resolveRedirect(Config.CHANNEL_VIDEO_STRAMING);
    }
}
