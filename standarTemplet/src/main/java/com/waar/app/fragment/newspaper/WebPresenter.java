package com.waar.app.fragment.newspaper;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.waar.app.fragment.BasePresenter;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class WebPresenter extends WebViewClient implements BasePresenter<WebPresentation> {
    private WebPresentation mPresentation;

    @Override
    public void onCreateView(@NonNull WebPresentation presentation) {
        mPresentation = presentation;
        mPresentation.toggleProgressBar(true);
        mPresentation.loadPage();
    }

    @Override
    public void onDestroyView() {
        mPresentation = null;
    }
    @Override
    public void onPageStarted(WebView view, String url,
                              Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        onPageFinished();
        super.onPageFinished(view, url);
    }

    @Override
    public void onReceivedError(WebView view, int errorCode,
                                String description, String failingUrl) {
        onError();
        super.onReceivedError(view, errorCode, description,
                failingUrl);
    }

    public void onError(){
        mPresentation.toggleProgressBar(false);
        // mPresentation.showError(App.getApp().getString(R.string.service_down));
    }

    public void onPageFinished(){
        mPresentation.toggleProgressBar(false);
    }
}
