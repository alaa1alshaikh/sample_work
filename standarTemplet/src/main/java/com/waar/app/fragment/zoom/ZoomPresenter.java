package com.waar.app.fragment.zoom;

import android.support.annotation.NonNull;

import com.waar.app.fragment.BasePresenter;

import java.util.Arrays;

/**
 * Created by Alaa Al-Shaikh on 2016-12-16.
 */
public class ZoomPresenter implements BasePresenter<ZoomPresentation> {

    private ZoomPresentation zoomPresentation;

    public void load(String image) {
        final String[] pics = image.split(";;");
        zoomPresentation.loadImages(Arrays.asList(pics));
    }

    @Override
    public void onCreateView(@NonNull ZoomPresentation presentation) {
        zoomPresentation = presentation;
    }

    @Override
    public void onDestroyView() {
        zoomPresentation = null;
    }
}
