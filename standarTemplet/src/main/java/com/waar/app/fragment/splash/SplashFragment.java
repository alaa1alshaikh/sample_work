package com.waar.app.fragment.splash;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waar.app.R;
import com.waar.app.fragment.BaseFragment;
import com.waar.app.permissions.DefaultPermissionProvider;
import com.waar.app.utils.Config;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class SplashFragment extends BaseFragment implements SplashPresentation {

    public static final String TAG = SplashFragment.class.getSimpleName();
    private static final int REQUEST_WRITE_AND_READ_EXTERNAL_STORAGE = 1000;

    SplashPresenter mPresenter;

    @Override
    protected boolean shouldShowActionBar() {
        return false;
    }

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new SplashPresenter();
        mPresenter.onCreateView(this);
        mPresenter.setPermissionProvider(new DefaultPermissionProvider(getActivity()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_splash, container, false);
        mPresenter.checkForStorageReadPermission();
        return view;
    }

    @Override
    protected boolean shouldLockDrawer() {
        return true;
    }

    @Override
    public void dismissSplash() {
        getNavigationManager().navigateToNewsWithAnimationScreen(Config.NEWS);
    }

    @Override
    public void showReadAndWriteExternalPermissionRequestDialog() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_WRITE_AND_READ_EXTERNAL_STORAGE);
    }

    @Override
    public void showReadAndWriteExternalPermissionRationaleDialog() {
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.write_and_read_external_storage_rationale_message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPresenter.requestReadExternalStoragePermission();
                    }
                })
                .create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_WRITE_AND_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mPresenter.splashScreen();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
