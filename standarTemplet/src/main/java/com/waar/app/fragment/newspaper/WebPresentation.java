package com.waar.app.fragment.newspaper;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public interface WebPresentation {

    void loadPage();

    void toggleProgressBar(boolean show);

    void showError(String message);
}
