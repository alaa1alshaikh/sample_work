package com.waar.app.adapter.slideshow;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.waar.app.R;
import com.waar.app.thirdParty.imageloader.ImageLoader;
import com.waar.app.thirdParty.views.ZoomAbleImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaa Al-Shaikh on 2016-12-16.
 */
public class ViewPhotoAdapter extends PagerAdapter {

    private List<String> images;
    private LayoutInflater inflater;
    private Context mContext;

    @BindView(R.id.zoom_image)
    ZoomAbleImageView image;

    @BindView(R.id.image_number)
    TextView imageNumber;

    private ImageLoader imageLoader;


    public ViewPhotoAdapter(List<String> images) {
        this.images = images;

    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mContext = container.getContext();
        inflater = LayoutInflater.from(mContext);
        if (imageLoader == null)
            imageLoader = new ImageLoader(mContext);
        View mView = (View) inflater
                .inflate(R.layout.zoomimage_item, null, false);
        ButterKnife.bind(this, mView);
        imageNumber.setText((position + 1) + "/" + (images.size()));
        updateImage(images.get(position));
        container.addView(mView);
        return mView;
    }

    void updateImage(String url) {
        imageLoader.DisplayImage(url, image, "",
                R.drawable.placeholder_image);
    }

}
