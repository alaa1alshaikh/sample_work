package com.waar.app.adapter.news_adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.waar.app.R;
import com.waar.app.entity.Data;
import com.waar.app.utils.Config;
import com.waar.app.thirdParty.views.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class NewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    @BindView(R.id.news_title)
    CustomTextView personNameAndTime;

    @BindView(R.id.news_description)
    CustomTextView commentSummery;

    @BindView(R.id.new_thumb)
    ImageView personThumb;

    @BindView(R.id.imageVideo)
    ImageView imageVideo;

    private Context context;
    private OnNewsClick onItemClick;
    private String id, title;


    public void setOnNewsClick(OnNewsClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public NewsHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void bindData(Data data) {
        id = data.getId();
        title = data.getTitle();
        personNameAndTime.setText(data.getTitle());
        commentSummery.setText(data.getDescription());
        if (data.getType().equalsIgnoreCase(Config.IMAGE)) {
            Picasso.with(context)
                    .load(data.getPath())
                    .placeholder(R.drawable.waar_placeholder_mainnews).into(personThumb);
            imageVideo.setVisibility(View.GONE);

        } else {
            Picasso.with(context)
                    .load(data.getPath())
                    .placeholder(R.drawable.waar_placeholder_mainnews).into(personThumb);
            imageVideo.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        if (onItemClick != null)
            onItemClick.newsItemClicked(id, title);
    }
}
