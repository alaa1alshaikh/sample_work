package com.waar.app.adapter.news_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waar.app.R;
import com.waar.app.entity.Data;
import com.waar.app.events.NewsClickEvent;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsHolder> implements OnNewsClick{

    private List<Data> news;

    public NewsAdapter(List<Data> news) {
        this.news = news;
    }

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);
        return new NewsHolder(root);
    }

    @Override
    public void onBindViewHolder(NewsHolder holder, int position) {
        holder.setOnNewsClick(this);
        holder.bindData(news.get(position));
    }

    public void addAll(List<Data> newNewsList){
        news.addAll(newNewsList);
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return news.size();
    }

    @Override
    public void newsItemClicked(String id, String title) {
        EventBus.getDefault().post(new NewsClickEvent(id, title));
    }
}
