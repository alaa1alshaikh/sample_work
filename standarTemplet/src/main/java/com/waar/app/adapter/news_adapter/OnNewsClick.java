package com.waar.app.adapter.news_adapter;

/**
 * Created by Alaa Al-Shaikh on 2016-12-16.
 */
public interface OnNewsClick {
    void newsItemClicked(String id, String title);
}
