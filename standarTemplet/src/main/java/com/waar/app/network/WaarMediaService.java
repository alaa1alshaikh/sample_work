package com.waar.app.network;

import com.waar.app.BuildConfig;
import com.waar.app.network.news.NewsApi;
import com.waar.app.utils.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class WaarMediaService {
    private NewsApi newsApi;

    private static WaarMediaService sService;

    public static WaarMediaService getInstance() {
        assert sService != null;
        return sService;
    }

    public static void initialize() {
        if (sService != null) {
            throw new IllegalStateException("WaarMediaService is already initialized!");
        }
        sService = new WaarMediaService();
    }

    public NewsApi getAuthApi() {
        return newsApi;
    }

    private WaarMediaService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ?
                HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.NEWS_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                //.addConverterFactory(converterFactory)
                .build();

        newsApi = retrofit.create(NewsApi.class);
    }

}
