package com.waar.app.network.news;

import android.support.annotation.NonNull;

import com.waar.app.entity.GetNews;
import com.waar.app.network.NetworkCallback;
import com.waar.app.network.NetworkResponse;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class NewsManger {
    private static final String TAG = NewsManger.class.getSimpleName();

    private NewsApi newsApi;

    @Inject
    public NewsManger(@NonNull NewsApi newsApi) {
        this.newsApi = newsApi;
    }

    public void getNews(@NonNull final NetworkCallback<GetNews> callback, String lastId, String lang, long id) {
        Call<GetNews> call = newsApi.getNews(lastId, lang, id);
        call.enqueue(new Callback<GetNews>() {
            @Override
            public void onResponse(Response<GetNews> response) {
                if (!response.isSuccess()) {
                    callback.onFailure(null);
                } else
                    callback.onResponse(new NetworkResponse<>(response));
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public void getNewsDetails(@NonNull final NetworkCallback<GetNews> callback, String id) {
        Call<GetNews> call = newsApi.getNewsDetails(id);
        call.enqueue(new Callback<GetNews>() {
            @Override
            public void onResponse(Response<GetNews> response) {
                if (!response.isSuccess()) {
                    callback.onFailure(null);
                } else
                    callback.onResponse(new NetworkResponse<>(response));
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }
}
