package com.waar.app.network.news;

import com.waar.app.entity.Data;
import com.waar.app.entity.GetNews;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public interface NewsApi {

    @Headers({"Content-Type: application/json"})
    @POST("/waarApp/api/fetchListNewsProcess.php")
    Call<GetNews> getNews(@Query("last_id") String lastId,
                                 @Query("lang") String lang,
                                 @Query("category_id") long categoryId);


    @Headers({"Content-Type: application/json"})
    @POST("/waarApp/api/fetchNewsProcess.php")
    Call<GetNews> getNewsDetails(@Query("news_id") String Id);


}
