package com.waar.app.network;

import android.support.annotation.NonNull;

import retrofit2.Response;

/**
 * Created by Tasneem Al-Shaikh on 2016-12-14.
 */
public class NetworkResponse<T> {
    private int mCode;
    private String mMessage;
    private T mBody;

    public NetworkResponse(@NonNull Response<T> response) {
        mCode = response.code();
        mMessage = response.message();
        mBody = response.body();
    }

    public int getCode() {
        return mCode;
    }

    public String getMessage() {
        return mMessage;
    }

    public T getBody() {
        return mBody;
    }
}
