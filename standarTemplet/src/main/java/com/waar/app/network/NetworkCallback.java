package com.waar.app.network;

import android.support.annotation.NonNull;

/**
 * Created by Tasneem Al-Shaikh on 2016-12-14.
 */
public interface NetworkCallback<T> {
    /**
     * Successful HTTP response.
     */
    void onResponse(@NonNull NetworkResponse<T> response);

    /**
     * Invoked when a network or unexpected exception occurred during the HTTP request.
     */
    void onFailure(@NonNull Throwable t);
}
