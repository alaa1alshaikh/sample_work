package com.waar.app.activity.navigation;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public interface NavigationManagerProvider {
    NavigationManager getNavigationManager();
}
