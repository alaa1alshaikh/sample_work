package com.waar.app.activity.navigation;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.animation.DecelerateInterpolator;

import com.waar.app.R;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class DrawerManager {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ActionBar mActionBar;
    private UpDisplayState mUpDisplayState = UpDisplayState.UNKNOWN;

    public enum UpDisplayState {
        UNKNOWN,
        UP,
        HAMBURGER,
        X
    }

    public DrawerManager(@NonNull DrawerLayout drawerLayout,
                         @NonNull ActionBarDrawerToggle drawerToggle,
                         @NonNull ActionBar actionBar) {
        mDrawerLayout = drawerLayout;
        mDrawerToggle = drawerToggle;
        mActionBar = actionBar;
    }


    /**
     * Locks or unlocks the navigation drawer. A locked drawer cannot be opened.
     *
     * @param lock Whether or not the drawer should be locked.
     */
    public void lockDrawer(boolean lock) {
        mDrawerLayout.setDrawerLockMode(lock ?
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED :
                DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void setToolbarTitle(String title) {
        mActionBar.setTitle(title);
    }

    public void setToolbarTitle(@StringRes int stringId) {
        mActionBar.setTitle(stringId);
    }

    public void showUpButton() {
        showUpButton(true);
    }

    public void showBurgerButton() {
        showUpButton(false);
    }

    private void showUpButton(boolean show) {
        if ((show && mUpDisplayState == UpDisplayState.UP) ||
                (!show && mUpDisplayState == UpDisplayState.HAMBURGER)) {
            return;
        }
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mUpDisplayState = show ? UpDisplayState.UP : UpDisplayState.HAMBURGER;

        ValueAnimator anim = ValueAnimator.ofFloat(show ? 0 : 1, show ? 1 : 0);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset);
            }
        });
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setDuration(500);
        anim.start();

        // Ensure correct indicator is showing
        toggleSync();
    }


    public void showXButton() {
        if (mUpDisplayState == UpDisplayState.X) {
            return;
        }
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        //mActionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        toggleSync();
        mUpDisplayState = UpDisplayState.X;
    }

    private void toggleSync() {
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public UpDisplayState getCurrentUpState() {
        return mUpDisplayState;
    }
}
