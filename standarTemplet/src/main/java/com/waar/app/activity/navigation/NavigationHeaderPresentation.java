package com.waar.app.activity.navigation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public interface NavigationHeaderPresentation {
    void showProfileName(@NonNull String firstName, @NonNull String lastName);
    void showProfileImage(@Nullable String imageUrl);
}
