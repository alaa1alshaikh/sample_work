package com.waar.app.activity.navigation;

import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;

import com.waar.app.R;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class NavigationPresenter implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private final NavigationPresentation mPresentation;

    public NavigationPresenter(NavigationPresentation presentation) {
        mPresentation = presentation;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_news:
                mPresentation.showNewsView();
                return true;
            case R.id.navigation_economy:
                mPresentation.showEconomyView();
                return true;
            case R.id.navigation_sports:
                mPresentation.showSportView();
                return true;
            case R.id.navigation_health:
                mPresentation.showHealthView();
                return true;
            case R.id.navigation_technology:
                mPresentation.showTechnologyView();
                return true;
            case R.id.navigation_culture:
                mPresentation.showCultureView();
                return true;
            case R.id.navigation_topics:
                mPresentation.showTopicsView();
                return true;
            case R.id.navigation_video:
                mPresentation.showVideoView();
                return true;
            case R.id.navigation_radio:
                mPresentation.showRadioView();
                return true;
            case R.id.navigation_contact_us:
                mPresentation.showContactUsView();
                return true;
            case R.id.navigation_language:
                mPresentation.showLanguageView();
                return true;
            case R.id.navigation_newspaper:
                mPresentation.showNewspaperView();
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.facebook_icon:
                mPresentation.showFacebookView();
                break;
            case R.id.twitter_icon:
                mPresentation.showTwitterView();
                break;
            case R.id.youtube_icon:
                mPresentation.showYoutubeView();
                break;
        }
    }
}
