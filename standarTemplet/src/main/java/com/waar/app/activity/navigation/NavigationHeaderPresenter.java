package com.waar.app.activity.navigation;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class NavigationHeaderPresenter {
    private final NavigationHeaderPresentation mPresentation;

    public NavigationHeaderPresenter(NavigationHeaderPresentation presentation) {
        mPresentation = presentation;
    }

    public void onProfileUpdated() {
        mPresentation.showProfileImage("");
        mPresentation.showProfileName("","");
    }
}
