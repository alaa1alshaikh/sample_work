package com.waar.app.activity.navigation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.waar.app.R;
import com.waar.app.entity.Data;
import com.waar.app.fragment.language.LanguageFragment;
import com.waar.app.fragment.news.NewsFragment;
import com.waar.app.fragment.news_deatails.NewsDetailsFragment;
import com.waar.app.fragment.newspaper.WebFragment;
import com.waar.app.fragment.radio.RadioFragment;
import com.waar.app.fragment.splash.SplashFragment;
import com.waar.app.fragment.video.VideoFragment;
import com.waar.app.fragment.zoom.ZoomFragment;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public class NavigationManager {
    private int mContainerId;
    private FragmentManager mFragmentManager;

    public NavigationManager(@NonNull FragmentManager fragmentManager, int containerId) {
        mFragmentManager = fragmentManager;
        mContainerId = containerId;
    }

    /**
     * Display a new fragment and add it to the back stack.
     *
     * @param fragment the fragment to show
     * @param tag      optional tag for the fragment to use for retrieving it later
     */
    public void addFragmentToBackStack(@NonNull Fragment fragment, @Nullable String tag) {
        mFragmentManager.beginTransaction()
                .addToBackStack(tag)
                .replace(mContainerId, fragment, tag)
                .commit();
    }

    /**
     * Replace the currently displayed fragment with another one.
     *
     * @param fragment the fragment to show
     * @param tag      optional tag for the fragment to use for retrieving it later
     */
    public void replaceFragment(@NonNull Fragment fragment, @Nullable String tag) {
        mFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mFragmentManager.beginTransaction()
                .replace(mContainerId, fragment, tag)
                .commit();
    }

    /**
     * Replaces all fragments in the backstack with another one.
     *
     * @param fragment the fragment to show
     * @param tag      optional tag for the fragment to use for retrieving it later
     */
    public void replaceBackStack(@NonNull Fragment fragment, @Nullable String tag) {
        clearBackStack();
        mFragmentManager.beginTransaction()
                .replace(mContainerId, fragment, tag)
                .commit();
    }

    /**
     * Replaces all fragments in the backstack with another one.
     *
     * @param fragment the fragment to show
     * @param tag      optional tag for the fragment to use for retrieving it later
     */
    public void addFragment(@NonNull Fragment fragment, @Nullable String tag) {
        clearBackStack();
        mFragmentManager.beginTransaction()
                .add(mContainerId, fragment, tag)
                .commit();
    }

    /**
     * Navigates to the previous screen.
     */
    public void popBackStack() {
        mFragmentManager.popBackStackImmediate();
    }

    /**
     * Remove all fragments (if any) in the backstack
     */
    public void clearBackStack() {
        if (mFragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backStackEntry = mFragmentManager.getBackStackEntryAt(0);
            if (backStackEntry != null) {
                mFragmentManager.popBackStackImmediate(backStackEntry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

    public void replaceFragmentWithAnimation(@NonNull Fragment fragment, @Nullable String tag) {
        mFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.zoom_enter, R.anim.zoom_exit)
                .replace(mContainerId, fragment, tag)
                .commit();
    }

    public void navigateToNewsScreen(long type) {
        NewsFragment fragment = NewsFragment.newInstance(type);
        addFragmentToBackStack(fragment, NewsFragment.TAG);
    }

    public void navigateToSplashScreen() {
        SplashFragment fragment = SplashFragment.newInstance();
        replaceFragment(fragment, SplashFragment.TAG);
    }

    public void navigateToVideoScreen() {
        VideoFragment fragment = VideoFragment.newInstance();
        addFragmentToBackStack(fragment, VideoFragment.TAG);
    }

    public void navigateToRadioScreen() {
        RadioFragment fragment = RadioFragment.newInstance();
        addFragmentToBackStack(fragment, RadioFragment.TAG);
    }

    public void navigateToLanguageScreen() {
        LanguageFragment fragment = LanguageFragment.newInstance();
        addFragmentToBackStack(fragment, LanguageFragment.TAG);
    }

    public void navigateToNewspaperScreen(String title, String url) {
        WebFragment fragment = WebFragment.newInstance(title, url);
        replaceFragment(fragment, WebFragment.TAG);
    }

    public void navigateToNewsWithAnimationScreen(long type) {
        NewsFragment fragment = NewsFragment.newInstance(type);
        replaceFragmentWithAnimation(fragment, NewsFragment.TAG);
    }

    public void navigateToArticleScreen(Data article) {
        NewsDetailsFragment fragment = NewsDetailsFragment.newInstance(article);
        addFragmentToBackStack(fragment, NewsDetailsFragment.TAG);
    }

    public void navigateToZoomImageScreen(String imageUrl) {
        ZoomFragment fragment = ZoomFragment.newInstance(imageUrl);
        addFragmentToBackStack(fragment, ZoomFragment.TAG);
    }

    public void navigateToSocialMediaScreen(String title, String url) {
        WebFragment fragment = WebFragment.newInstance(title, url);
        addFragmentToBackStack(fragment, WebFragment.TAG);
    }

    public void navigateToArticleScreen(String id, String title) {
        NewsDetailsFragment fragment = NewsDetailsFragment.newInstance(id, title);
        addFragmentToBackStack(fragment, NewsDetailsFragment.TAG);
    }


}
