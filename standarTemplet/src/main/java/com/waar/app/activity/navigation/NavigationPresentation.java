package com.waar.app.activity.navigation;

/**
 * Created by Alaa Al-Shaikh on 2016-12-14.
 */
public interface NavigationPresentation {
    void showNewsView();

    void showHealthView();

    void showEconomyView();

    void showSportView();

    void showTechnologyView();

    void showCultureView();

    void showTopicsView();

    void showVideoView();

    void showRadioView();

    void showLanguageView();

    void showContactUsView();

    void showNewspaperView();

    void showFacebookView();

    void showTwitterView();

    void showYoutubeView();


}
