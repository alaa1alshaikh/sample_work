package com.waar.app.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.waar.app.R;
import com.waar.app.activity.navigation.DrawerManager;
import com.waar.app.activity.navigation.DrawerManagerProvider;
import com.waar.app.activity.navigation.NavigationManager;
import com.waar.app.activity.navigation.NavigationManagerProvider;
import com.waar.app.activity.navigation.NavigationPresentation;
import com.waar.app.activity.navigation.NavigationPresenter;
import com.waar.app.utils.Config;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements
        DrawerManagerProvider,
        NavigationManagerProvider,
        NavigationPresentation {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerManager mDrawerManager;
    private NavigationManager mNavigationManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.navigation_drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    ImageView facebookBtn, twitterBtn, youtubeBtn;
    private boolean isRecreated = false;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(Config.LANGUAGE, true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            isRecreated = savedInstanceState.getBoolean(Config.LANGUAGE);
        }
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(Config.LANGUAGE))
                isRecreated = getIntent().getExtras().getBoolean(Config.LANGUAGE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        setup();
    }

    private void setup() {
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        mNavigationManager = new NavigationManager(getSupportFragmentManager(), R.id.container_view);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.open_drawer,
                R.string.close_drawer);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set a listener for when the toggle is disabled
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mDrawerManager = new DrawerManager(mDrawerLayout, mDrawerToggle, getSupportActionBar());

        NavigationPresenter navigationPresenter = new NavigationPresenter(this);
        mNavigationView.setNavigationItemSelectedListener(navigationPresenter);
        mNavigationView.setItemIconTintList(null);
        View header = mNavigationView.getHeaderView(0);


        facebookBtn = (ImageView) header.findViewById(R.id.facebook_icon);
        youtubeBtn = (ImageView) header.findViewById(R.id.youtube_icon);
        twitterBtn = (ImageView) header.findViewById(R.id.twitter_icon);

        facebookBtn.setOnClickListener(navigationPresenter);
        youtubeBtn.setOnClickListener(navigationPresenter);
        twitterBtn.setOnClickListener(navigationPresenter);

        if (!isRecreated)
            getNavigationManager().navigateToSplashScreen();
        else
            getNavigationManager().navigateToNewsScreen(Config.NEWS);


    }

    @Override
    public DrawerManager getDrawerManager() {
        return mDrawerManager;
    }

    @Override
    public NavigationManager getNavigationManager() {
        return mNavigationManager;
    }

    @Override
    public void showNewsView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToNewsScreen(Config.NEWS);
    }

    @Override
    public void showHealthView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToNewsScreen(Config.HEALTH);
    }

    @Override
    public void showEconomyView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToNewsScreen(Config.ECONOMY);
    }

    @Override
    public void showSportView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToNewsScreen(Config.SPORTS);
    }

    @Override
    public void showTechnologyView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToNewsScreen(Config.TECHNOLOGY);
    }

    @Override
    public void showCultureView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToNewsScreen(Config.ART_CULTURE);
    }

    @Override
    public void showTopicsView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToNewsScreen(Config.OTHER_NEWS);
    }

    @Override
    public void showVideoView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToVideoScreen();
    }

    @Override
    public void showRadioView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToRadioScreen();
    }

    @Override
    public void showLanguageView() {
        mDrawerLayout.closeDrawers();
        mNavigationManager.navigateToLanguageScreen();
    }

    @Override
    public void showContactUsView() {
        mDrawerLayout.closeDrawers();
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
                Uri.fromParts("mailto", Config.CONTACT_US_EMAIL, null));
        startActivity(Intent.createChooser(emailIntent,
                getString(R.string.send_mail)));
    }

    @Override
    public void showNewspaperView() {
        mDrawerLayout.closeDrawers();
        getNavigationManager().navigateToNewspaperScreen(getString(R.string.newspaper), Config.NEWS_PAPER);
    }

    @Override
    public void showFacebookView() {
        mDrawerLayout.closeDrawers();
        getNavigationManager().navigateToSocialMediaScreen(Config.FACEBOOK, Config.FACEBOOK_URL);
    }

    @Override
    public void showTwitterView() {
        mDrawerLayout.closeDrawers();
        getNavigationManager().navigateToSocialMediaScreen(Config.TWITTER, Config.TWITTER_URL);
    }

    @Override
    public void showYoutubeView() {
        mDrawerLayout.closeDrawers();
        getNavigationManager().navigateToSocialMediaScreen(Config.YOUTUBE, Config.YOUTUBE_URL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragmentList) {
            // Sometimes getFragments() gives us a list with null references in it...
            // This makes sure we are actually hitting a fragment.
            if (fragment != null) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerManager.getCurrentUpState() != DrawerManager.UpDisplayState.HAMBURGER) {
                    super.onBackPressed();
                    return true;
                }
                return mDrawerToggle.onOptionsItemSelected(item);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
            //  getNavigationManager().popBackStack();
        }
    }

}
