package com.waar.app.events;

/**
 * Created by Alaa Al-Shaikh on 2016-12-16.
 */
public class NewsClickEvent {
    private String id;
    private String title;

    public NewsClickEvent(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }
}
