package com.waar.app.thirdParty.imageloader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.waar.app.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {

    static MemoryCache memoryCache = new MemoryCache();

    FileCache fileCache;
    private Map<ImageView, String> imageViews = Collections
            .synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService;
    Animation anim_in;

    public ImageLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
        anim_in = AnimationUtils.loadAnimation(context, R.anim.fade_in_image);

    }

    int stub_id = 0;

    // final int stub_id2 =R.drawable.bg_default_temp_image;

    public void DisplayImage(String url, ImageView imageView,
                             String placeHolderImageFromCashedFile, int imageResourceID_PH) {

        if (imageResourceID_PH == -1) {// come from imagePager
            stub_id = 0;
        }
        if (imageView != null) {
            imageView.startAnimation(anim_in);
            imageViews.put(imageView, url);

            Bitmap bitmap = memoryCache.get(url);

            if (bitmap != null)

                imageView.setImageBitmap(memoryCache.get(url));

            else {

                queuePhoto(url, imageView, placeHolderImageFromCashedFile,
                        imageResourceID_PH);

                if (placeHolderImageFromCashedFile.trim().length() != 0) {

                    Bitmap bitmap2 = memoryCache
                            .get(placeHolderImageFromCashedFile);

                    if (bitmap2 != null) {
                        imageView.setImageBitmap(bitmap2);
                    } else {
                        imageView.setImageResource(imageResourceID_PH);

                    }
                } else if (imageResourceID_PH != 0) {
                    imageView.setImageResource(imageResourceID_PH);
                } else {
                    imageView.setImageResource(stub_id);

                }

            }
        }
        // else {************
        //
        // queuePhoto(url, null, "", 0);
        //
        // }
    }

    // public static void getClickedImageBytes(String url, ImageView imageView,
    // Context context) {
    // try {
    // Bitmap bitmap = memoryCache.get(url);
    //
    // File cacheDir = null;
    // cacheDir = new File(
    // android.os.Environment.getExternalStorageDirectory(),
    // "Doo SMS");
    //
    // String path = Environment.getExternalStorageDirectory().toString();
    // File file = new File(cacheDir + File.separator + "temp.jpg");
    // file.createNewFile();
    // FileOutputStream out = new FileOutputStream(file.getAbsolutePath());
    // bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    //
    // }

    private void queuePhoto(String url, ImageView imageView,
                            String placeHolderImageFromCashedFile, int imageResourceID_PH) {

        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p,
                placeHolderImageFromCashedFile, imageResourceID_PH));
    }

    private Bitmap getBitmap(String url) {
        File f = fileCache.getFile(url);

        // from SD cache
        Bitmap b = decodeFile(f);
        if (b != null)
            return b;

        // from web
        try {
            Bitmap bitmap = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl
                    .openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            bitmap = decodeFile(f);
            return bitmap;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    // // decodes image and scales it to reduce memory consumption
    // private Bitmap decodeFile(File f) {
    // try {
    // // decode image size
    // BitmapFactory.Options o = new BitmapFactory.Options();
    // o.inJustDecodeBounds = true;
    //
    // BitmapFactory.decodeStream(new FileInputStream(f), null, o);
    // // Find the correct scale value. It should be the power of 2.
    // final int REQUIRED_SIZE = 100;
    // int width_tmp = o.outWidth, height_tmp = o.outHeight;
    // int scale = 1;
    // while (true) {
    // if (width_tmp / 2 < REQUIRED_SIZE
    // || height_tmp / 2 < REQUIRED_SIZE)
    // break;
    // width_tmp /= 2;
    // height_tmp /= 2;
    // scale *= 2;
    // }
    // // decode with inSampleSize
    // BitmapFactory.Options o2 = new BitmapFactory.Options();
    // o2.inSampleSize = scale;
    //
    // return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
    // } catch (FileNotFoundException e) {
    // }
    // return null;
    // }

    // decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        try {
            // // decode image size
            // BitmapFactory.Options o = new BitmapFactory.Options();
            // o.inJustDecodeBounds = true;
            // BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            // // Find the correct scale value. It should be the power of 2.
            // final int REQUIRED_SIZE = 70;
            // int width_tmp = o.outWidth, height_tmp = o.outHeight;
            // int scale = 1;
            // while (true) {
            // if (width_tmp / 2 < REQUIRED_SIZE
            // || height_tmp / 2 < REQUIRED_SIZE)
            // break;
            // width_tmp /= 2;
            // height_tmp /= 2;
            // scale *= 2;
            // }
            // // decode with inSampleSize
            // BitmapFactory.Options o2 = new BitmapFactory.Options();
            // o2.inSampleSize = scale;
            //
            // // Bitmap bitmap=getRoundedCornerBitmap(
            // // BitmapFactory.decodeStream(new FileInputStream(f)), 50);
            // // null,
            // // o2
            return BitmapFactory.decodeStream(new FileInputStream(f));
        } catch (FileNotFoundException e) {

        }
        return null;
    }

    // Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;
        String placeHolderImageFromCashedFile = "";
        int imageResourceID_PH = 0;

        PhotosLoader(PhotoToLoad photoToLoad,
                     String placeHolderImageFromCashedFile, int imageResourceID_PH) {
            this.photoToLoad = photoToLoad;
            this.imageResourceID_PH = imageResourceID_PH;
            this.placeHolderImageFromCashedFile = placeHolderImageFromCashedFile;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            Bitmap bmp = getBitmap(photoToLoad.url);
            memoryCache.put(photoToLoad.url, bmp);
            if (imageViewReused(photoToLoad))
                return;
            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad,
                    placeHolderImageFromCashedFile, imageResourceID_PH);

            try {

                Activity a = (Activity) photoToLoad.imageView.getContext();
                a.runOnUiThread(bd);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        String placeHolderImageFromCashedFile = "";
        int imageResourceID_PH = 0;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p,
                               String placeHolderImageFromCashedFile, int imageResourceID_PH) {
            bitmap = b;
            photoToLoad = p;
            this.imageResourceID_PH = imageResourceID_PH;
            this.placeHolderImageFromCashedFile = placeHolderImageFromCashedFile;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            if (bitmap != null) {
                photoToLoad.imageView.setImageBitmap(bitmap);

            } else {

                if (placeHolderImageFromCashedFile.trim().length() != 0) {
                    Bitmap bitmap2 = memoryCache
                            .get(placeHolderImageFromCashedFile);

                    if (bitmap2 != null) {
                        photoToLoad.imageView.setImageBitmap(bitmap2);
                    } else {
                        photoToLoad.imageView
                                .setImageResource(imageResourceID_PH);

                    }
                } else if (imageResourceID_PH != 0) {
                    photoToLoad.imageView.setImageResource(imageResourceID_PH);
                } else {
                    photoToLoad.imageView.setImageResource(stub_id);

                }
            }

        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    public Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        try {

            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(),
                    bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = pixels;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        } catch (Exception e) {
            return null;
        }
    }

}
