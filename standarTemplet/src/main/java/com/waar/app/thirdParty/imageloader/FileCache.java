package com.waar.app.thirdParty.imageloader;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class FileCache {

    private File cacheDir;

    public FileCache(Context context) {
        // Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {

            cacheDir = new File(
                    android.os.Environment.getExternalStorageDirectory(),
                    ".WAAR");

            if (dirSize(cacheDir) > 104857600) {// 10485760 ifExceesed 10MB
                // /20971520 20mb ,104857600
                // 100mb
                clear();
            }

        } else {

            cacheDir = context.getCacheDir();
            if (dirSize(cacheDir) > 1048576) {// ifExceesed
                // 1MB
                clear();
            }
        }
        if (!cacheDir.exists())
            cacheDir.mkdirs();
    }

    /**
     * Return the size of a directory in bytes
     */
    private static long dirSize(File dir) {

        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if (fileList[i].isDirectory()) {
                    result += dirSize(fileList[i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                }
            }
            return result; // return the file size
        }
        return 0;
    }

    File getFile(String url) {

        String filename = String.valueOf(url.hashCode());

        File f = new File(cacheDir, filename);
        return f;

    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files)
            f.delete();
    }

    public File getFileDirection() {

        return cacheDir;
    }

    public Bitmap decodeFile(File f, int scaleSize) {
        try {

            Bitmap bitmap = BitmapFactory.decodeFile(f.getPath());

            int width_tmp = bitmap.getWidth(), height_tmp = bitmap.getHeight();

            if ((height_tmp > width_tmp) && (height_tmp > scaleSize)) {

                int width = (int) width_tmp * scaleSize / height_tmp;

                return (Bitmap.createScaledBitmap(bitmap, width, scaleSize,
                        false));

            } else if ((width_tmp > height_tmp) && (width_tmp > scaleSize)) {

                int height = (int) height_tmp * scaleSize / width_tmp;

                return (Bitmap.createScaledBitmap(bitmap, scaleSize, height,
                        false));

            } else if ((height_tmp == width_tmp) && (height_tmp > scaleSize)) {

                return (Bitmap.createScaledBitmap(bitmap, scaleSize, scaleSize,
                        false));

            } else {// LessThanDererminedSize
                return bitmap;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

}