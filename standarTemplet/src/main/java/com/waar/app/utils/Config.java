package com.waar.app.utils;

/**
 * @author noor
 */
public class Config {

    public static final int LIMIT_S = 10;
    public static final int LIMIT_M = 15;
    public static final int LIMIT_L = 20;

    public static final String FACEBOOK_URL = "https://web.facebook.com/WAARmediaNetwork?_rdr";
    public static final String FACEBOOK = "Facebook";
    public static final String TWITTER_URL = "http://twitter.com/WAARMedia";// done
    public static final String TWITTER = "Twitter";// done
    public static final String YOUTUBE_URL = "http://www.youtube.com/c/waarmedianew";// done
    public static final String YOUTUBE= "Youtube";// done

    public static final String GooglePlus = "https://plus.google.com/u/0/105610248574846150403/posts";// done
    public static final String CONTACT_US_EMAIL = "info@waarmedia.com";// done
    public static String CHANNEL_AUDIO_STRAMING = "http://waarmedia.com/waarApp/radioURL.php?os=android";// done
    public static final String CHANNEL_VIDEO_STRAMING = "http://www.waarmedia.com/waarApp/liveStramURL.php?os=android";// done
    public static final String addTokenProcess = "http://waarmedia.com/waarApp/api/addTokenProcess.php?";// done

    public static final String GET_NEWS = "http://waarmedia.com/waarApp/api/fetchListNewsProcess.php?";// done
    public static final String GET_SOME_NEWS = "http://waarmedia.com/waarApp/api/fetchNewsProcess.php?";// done

    public static final String NEWS_PAPER = "http://waarmedia.com/waarApp/newsPaper.php?";// done


    public static final String NEWS_URL = "http://waarmedia.com/";// done


    public static final String TYPE = "type";// done
    public static final long ECONOMY = 7;
    public static final long NEWS = 6;
    public static final long OTHER_NEWS = 5;
    public static final long ART_CULTURE = 4;
    public static final long TECHNOLOGY = 3;
    public static final long HEALTH = 2;
    public static final long SPORTS = 1;

    public static final String ARABIC = "ar";
    public static final String ENGLISH = "en";
    public static final String TURKISH = "tr";
    public static final String KURDISH = "ur";

    public static final String TURKISH_CKB = "ckb";
    public static final String KURDISH_CKB= "ckb_ar";

    public static final String TITLE = "TITLE";// done
    public static final String URL = "URL";// done


    public static final String IMAGE = "1";
    public static final String VIDEO = "2";

    public static String ARTICLE_DETAILS ="ARTICLE_DETAILS";
    public static String ARTICLE_DETAILS_ID ="ARTICLE_ID";
    public static String ARTICLE_DETAILS_TITLE="ARTICLE_TITLE";

    public static final String IMAGE_URL ="IMAGE_URL" ;
    public static final String LANGUAGE ="LANGUAGE" ;



// Sport=1
    // Health=2
    // Technology=3
    // Arts & Culture=4
    // Variety Of Topics=5
    // News=6

}
