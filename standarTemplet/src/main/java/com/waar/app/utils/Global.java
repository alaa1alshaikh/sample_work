package com.waar.app.utils;

import android.content.Context;
import android.content.Intent;

import com.waar.app.R;
import com.waar.app.entity.Data;

import java.io.Closeable;

public class Global {

    public static void invitePeople(Context context, Data news) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                news.getTitle());
        sharingIntent.putExtra(
                android.content.Intent.EXTRA_TEXT,
                news.getDescription() + "\n"
                        + context.getString(R.string.invitationText));
        context.startActivity(sharingIntent);
    }

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static void closeQuietly(Closeable closeable) {
        try {
            closeable.close();
        } catch (Exception ex) {
        }
    }

}
