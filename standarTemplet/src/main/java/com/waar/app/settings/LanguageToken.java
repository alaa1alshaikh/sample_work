package com.waar.app.settings;

import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class LanguageToken {
    private static final String LOCAL_TOKEN_KEY = "local_token";
    private static final String SERVER_TOKEN_KEY = "server_token";


    private SharedPreferences mSharedPreferences;
    private String mLocalToken;
    private String mServerToken;

    public LanguageToken(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public void clearTokens() {
        mLocalToken = null;
        mSharedPreferences.edit()
                .remove(LOCAL_TOKEN_KEY)
                .remove(SERVER_TOKEN_KEY)
                .apply();
    }


    public String getLocalLanguage() {
        if (TextUtils.isEmpty(mLocalToken)) {
            mLocalToken = mSharedPreferences.getString(LOCAL_TOKEN_KEY, "ur");
        }
        return mLocalToken;
    }

    public void setLocalLanguage(String localToken) {
        mSharedPreferences.edit().putString(LOCAL_TOKEN_KEY, localToken).apply();
        mLocalToken = localToken;
    }

    public String getServerLanguage() {
        if (TextUtils.isEmpty(mServerToken)) {
            mServerToken = mSharedPreferences.getString(SERVER_TOKEN_KEY, "ckb_ar");
        }
        return mServerToken;
    }

    public void setServerLanguage(String serverToken) {
        mSharedPreferences.edit().putString(SERVER_TOKEN_KEY, serverToken).apply();
        mServerToken = serverToken;
    }
}
