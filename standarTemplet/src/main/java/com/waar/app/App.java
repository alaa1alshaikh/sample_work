package com.waar.app;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.waar.app.dagger.component.DaggerNetworkComponent;
import com.waar.app.dagger.component.DaggerNewsComponent;
import com.waar.app.dagger.component.NetworkComponent;
import com.waar.app.dagger.component.NewsComponent;
import com.waar.app.dagger.module.AppModule;
import com.waar.app.dagger.module.NetworkModule;
import com.waar.app.dagger.module.NewsModule;
import com.waar.app.thirdParty.views.FontsOverride;

import java.util.Locale;

/**
 * Created by Alaa Al-Shaikh on 2016-12-15.
 */
public class App extends Application {

    NetworkComponent networkComponent;
    NewsComponent newsComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        networkComponent = DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule())
                .appModule(new AppModule(this))
                .build();

        newsComponent = DaggerNewsComponent.builder()
                .networkComponent(networkComponent)
                .newsModule(new NewsModule())
                .build();

        loadUserLang(this);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/DroidKufi-Bold.ttf");

    }

    public void loadUserLang(Context context) {
        String languageToLoad = networkComponent.languageToken().getLocalLanguage();
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }

    public NetworkComponent getNetworkComponent() {
        return networkComponent;
    }

    public NewsComponent getNewsComponent() {
        return newsComponent;
    }
}
