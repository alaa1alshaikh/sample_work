package com.waar.app.fragment.newspaper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Alaa Al-Shaikh on 2016-12-29.
 */
@RunWith(MockitoJUnitRunner.class)
public class WebPresenterTest {

    private WebPresenter mPresenter;

    @Mock
    private WebPresentation mPresentation;

    @Before
    public void setUp() {
        mPresenter = new WebPresenter();
        mPresenter.onCreateView(mPresentation);
    }

    @Test
    public void onCreateView() {
        verify(mPresentation, times(1)).toggleProgressBar(true);
        verify(mPresentation, times(1)).loadPage();
    }

    @Test
    public void onError_ReceivedError() {
        mPresenter.onError();
        verify(mPresentation, times(1)).toggleProgressBar(false);
        // verify(mPresentation, times(1)).showError("");

    }

    @Test
    public void onPageFinished_WebView() {
        mPresenter.onPageFinished();
        verify(mPresentation, times(1)).toggleProgressBar(false);
    }
}