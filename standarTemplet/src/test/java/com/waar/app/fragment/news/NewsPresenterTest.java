package com.waar.app.fragment.news;

import com.waar.app.entity.GetNews;
import com.waar.app.network.NetworkCallback;
import com.waar.app.network.news.NewsManger;
import com.waar.app.settings.LanguageToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Alaa Al-Shaikh on 2016-12-29.
 */


@RunWith(MockitoJUnitRunner.class)
public class NewsPresenterTest {

    private static long NEWS_ID = 7;
    private static String LAST_ID = "0";

    private NewsPresenter presenter;

    @Mock
    private NewsPresentation presentation;

    @Mock
    private NewsManger newsManger;

    @Mock
    private LanguageToken languageToken;

    @Before
    public void setUp() throws Exception {
        presenter = new NewsPresenter(newsManger, languageToken);
        presenter.onCreateView(presentation);
    }

    @Test
    public void fetchNewsListFromId_Test() {
        presenter.fetchNewsListFromId(NEWS_ID);
        verify(newsManger, times(1)).getNews(Matchers.<NetworkCallback<GetNews>>any(), eq(LAST_ID), anyString(),eq(NEWS_ID));
    }
}