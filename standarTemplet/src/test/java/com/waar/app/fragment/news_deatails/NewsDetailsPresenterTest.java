package com.waar.app.fragment.news_deatails;

import com.waar.app.entity.GetNews;
import com.waar.app.network.NetworkCallback;
import com.waar.app.network.news.NewsManger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Alaa Al-Shaikh on 2016-12-29.
 */

@RunWith(MockitoJUnitRunner.class)
public class NewsDetailsPresenterTest {

    private static String NEWS_ID = "10868";
    NewsDetailsPresenter presenter;

    @Mock
    private NewsDetailsPresentation newsDetailsPresentation;

    @Mock
    private NewsManger newsManger;

    @Before
    public void setUp() throws Exception {
        presenter = new NewsDetailsPresenter(newsManger);
        presenter.onCreateView(newsDetailsPresentation);
    }

    @Test
    public void fetchNewsFromId_Deatails() {
        presenter.fetchNewsFromId(NEWS_ID);
        verify(newsManger, times(1)).getNewsDetails(Matchers.<NetworkCallback<GetNews>>any(), eq(NEWS_ID));
    }

    @Test
    public void openVideo_Click() {
        presenter.setVideo(true);
        presenter.onClickPerform();
        verify(newsDetailsPresentation, times(1)).openVideo();
    }

    @Test
    public void openZoomImage_Click() {
        presenter.setVideo(false);
        presenter.onClickPerform();
        verify(newsDetailsPresentation, times(1)).openZoomView();
    }

}