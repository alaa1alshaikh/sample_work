package com.waar.app.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waar.app.entity.deserializer.GetNewsDeserializer;
import com.waar.app.utils.Global;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Alaa Al-Shaikh on 2016-12-29.
 */
@RunWith(MockitoJUnitRunner.class)
public class GetNewsModelTest {

    @Test
    public void GetNewsModelFromJson() {
        InputStream is =
                GetNewsModelTest.class.getResourceAsStream("/entity/NewsModel.json");
        String json = Global.convertStreamToString(is);
        Global.closeQuietly(is);
        Gson gson = new GsonBuilder().registerTypeAdapter(GetNews.class, new GetNewsDeserializer()).create();
        GetNews getNews = gson.fromJson(json, GetNews.class);
        assertThat(getNews).isNotNull();
        Data data = getNews.getData().get(0);
        assertThat(data.getId()).isEqualTo("10868");
        assertThat(data.getTitle()).isEqualTo("حاكم شيخ لطيف : مشروع تعامل بغداد مع محافظات الاقليم غير دستورية");
        assertThat(data.getDescription()).isEqualTo("waar –  دهوك : \\r\\n\\r\\nأعلن خبير في الدستور العراقي، ونائب سابق في حركة التغيير؛إن تعامل الحكومة العراقية المباشر مع محافظات الاقليم غير دستورية وغير مقبولة.\\r\\n\\r\\n\\r\\nوقال حاكم شيخ لطيف، العضو السابق في مجلس النواب العراقي : اذا ما ما تم التصويت على مشروع تعامل بغداد المباشر وتم تصديقه، فسيتم رفضه في المحكمة الفدرالية، اذا ما تم الطعن فيه.لان المادة 117 من الدستور العراقي ينص على عدم امكانية بغداد في التعامل المباشر مع محافظات الإقليم.\\r\\n\\r\\n\\r\\nوفي بداية الشهر الجاري، أعلن النائب الثاني لرئيس مجلس النواب العراقي في السليمانية مشروع تعامل الحكومة العراقية المباشر مع محافظات الاقليم لإرسال رواتب الموظفين، ويدعم المشروع المعلن عنه؛ كل من حركة التغيير، الاتحاد الوطني والجماعة الإسلامية.\\r\\n\\r\\n");
        assertThat(data.getPath()).isEqualTo("http://waarmedia.com/file/2016/10/facebooknewswaar.jpg");
        assertThat(data.getType()).isEqualTo("1");
    }

}