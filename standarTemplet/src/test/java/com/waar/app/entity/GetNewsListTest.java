package com.waar.app.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waar.app.entity.deserializer.GetNewsDeserializer;
import com.waar.app.utils.Global;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Alaa Al-Shaikh on 2016-12-29.
 */
@RunWith(MockitoJUnitRunner.class)
public class GetNewsListTest {

    @Test
    public void GetNewsListTestFromJson() {
        InputStream is =
                GetNewsListTest.class.getResourceAsStream("/entity/NewsList.json");
        String json = Global.convertStreamToString(is);
        Global.closeQuietly(is);
        Gson gson = new GsonBuilder().registerTypeAdapter(GetNews.class, new GetNewsDeserializer()).create();
        GetNews getNews = gson.fromJson(json, GetNews.class);
        assertThat(getNews).isNotNull();
        assertThat(getNews.getData().size()).isEqualTo(10);


        Data data = getNews.getData().get(0);
        assertThat(data.getId()).isEqualTo("11224");
        assertThat(data.getTitle()).isEqualTo("���� �?��?�?�� ��� ?� �?�Ԙ��? � �����? Ș�� ��?�?�");
        assertThat(data.getDescription()).isEqualTo("waar �  ���� : \\r\\n\\r\\n�?��?�?�? ���?? �?���? �ǐ?���Ϻ");
        assertThat(data.getPath()).isEqualTo("http://waarmedia.com/file/2016/11/adfisis.jpg");
        assertThat(data.getType()).isEqualTo("1");


        Data data2 = getNews.getData().get(1);
        assertThat(data2.getId()).isEqualTo("10895");
        assertThat(data2.getTitle()).isEqualTo("����� ��?�: ���� � ��\u200C���\u200C���? ���� ��\u200C���\u200C� ����� ���\u200C�");
        assertThat(data2.getDescription()).isEqualTo("waar �  ���� :\\r\\n\\r\\n�?������ ��\u200C��\u200C��\u200C���� ��\u200C�?� �?�?��?");
        assertThat(data2.getPath()).isEqualTo("http://waarmedia.com/file/2016/11/adfisis.jpg");
        assertThat(data2.getType()).isEqualTo("1");
    }
}
