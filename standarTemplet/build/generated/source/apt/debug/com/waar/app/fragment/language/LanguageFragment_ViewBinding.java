// Generated code from Butter Knife. Do not modify!
package com.waar.app.fragment.language;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.waar.app.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LanguageFragment_ViewBinding<T extends LanguageFragment> implements Unbinder {
  protected T target;

  @UiThread
  public LanguageFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.englishLayout = Utils.findRequiredViewAsType(source, R.id.english_lay, "field 'englishLayout'", RelativeLayout.class);
    target.arabicLayout = Utils.findRequiredViewAsType(source, R.id.arabic_lay, "field 'arabicLayout'", RelativeLayout.class);
    target.turkishLayout = Utils.findRequiredViewAsType(source, R.id.turkish_lay, "field 'turkishLayout'", RelativeLayout.class);
    target.kurdishLayout = Utils.findRequiredViewAsType(source, R.id.kordi_lay, "field 'kurdishLayout'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.englishLayout = null;
    target.arabicLayout = null;
    target.turkishLayout = null;
    target.kurdishLayout = null;

    this.target = null;
  }
}
