package com.waar.app.dagger.module;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import okhttp3.logging.HttpLoggingInterceptor;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvideHttpLoggingInterceptorFactory
    implements Factory<HttpLoggingInterceptor> {
  private final NetworkModule module;

  public NetworkModule_ProvideHttpLoggingInterceptorFactory(NetworkModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public HttpLoggingInterceptor get() {
    return Preconditions.checkNotNull(
        module.provideHttpLoggingInterceptor(),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<HttpLoggingInterceptor> create(NetworkModule module) {
    return new NetworkModule_ProvideHttpLoggingInterceptorFactory(module);
  }

  /** Proxies {@link NetworkModule#provideHttpLoggingInterceptor()}. */
  public static HttpLoggingInterceptor proxyProvideHttpLoggingInterceptor(NetworkModule instance) {
    return instance.provideHttpLoggingInterceptor();
  }
}
