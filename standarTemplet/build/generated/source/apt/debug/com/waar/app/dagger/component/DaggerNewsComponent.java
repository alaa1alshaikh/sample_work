package com.waar.app.dagger.component;

import com.waar.app.dagger.module.NewsModule;
import com.waar.app.dagger.module.NewsModule_ProvideNewsApiFactory;
import com.waar.app.dagger.module.NewsModule_ProvideNewsDetailsPresenterFactory;
import com.waar.app.dagger.module.NewsModule_ProvideNewsPresenterFactory;
import com.waar.app.fragment.news.NewsPresenter;
import com.waar.app.fragment.news_deatails.NewsDetailsPresenter;
import com.waar.app.network.news.NewsApi;
import com.waar.app.settings.LanguageToken;
import dagger.internal.DoubleCheck;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerNewsComponent implements NewsComponent {
  private Provider<Retrofit> retrofitProvider;

  private Provider<NewsApi> provideNewsApiProvider;

  private Provider<LanguageToken> languageTokenProvider;

  private Provider<NewsPresenter> provideNewsPresenterProvider;

  private Provider<NewsDetailsPresenter> provideNewsDetailsPresenterProvider;

  private DaggerNewsComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.retrofitProvider =
        new Factory<Retrofit>() {
          private final NetworkComponent networkComponent = builder.networkComponent;

          @Override
          public Retrofit get() {
            return Preconditions.checkNotNull(
                networkComponent.retrofit(),
                "Cannot return null from a non-@Nullable component method");
          }
        };

    this.provideNewsApiProvider =
        DoubleCheck.provider(
            NewsModule_ProvideNewsApiFactory.create(builder.newsModule, retrofitProvider));

    this.languageTokenProvider =
        new Factory<LanguageToken>() {
          private final NetworkComponent networkComponent = builder.networkComponent;

          @Override
          public LanguageToken get() {
            return Preconditions.checkNotNull(
                networkComponent.languageToken(),
                "Cannot return null from a non-@Nullable component method");
          }
        };

    this.provideNewsPresenterProvider =
        DoubleCheck.provider(
            NewsModule_ProvideNewsPresenterFactory.create(
                builder.newsModule, provideNewsApiProvider, languageTokenProvider));

    this.provideNewsDetailsPresenterProvider =
        DoubleCheck.provider(
            NewsModule_ProvideNewsDetailsPresenterFactory.create(
                builder.newsModule, provideNewsApiProvider));
  }

  @Override
  public NewsPresenter provideNewsPresenter() {
    return provideNewsPresenterProvider.get();
  }

  @Override
  public NewsDetailsPresenter provideNewsDetailsPresenter() {
    return provideNewsDetailsPresenterProvider.get();
  }

  public static final class Builder {
    private NewsModule newsModule;

    private NetworkComponent networkComponent;

    private Builder() {}

    public NewsComponent build() {
      if (newsModule == null) {
        this.newsModule = new NewsModule();
      }
      if (networkComponent == null) {
        throw new IllegalStateException(NetworkComponent.class.getCanonicalName() + " must be set");
      }
      return new DaggerNewsComponent(this);
    }

    public Builder newsModule(NewsModule newsModule) {
      this.newsModule = Preconditions.checkNotNull(newsModule);
      return this;
    }

    public Builder networkComponent(NetworkComponent networkComponent) {
      this.networkComponent = Preconditions.checkNotNull(networkComponent);
      return this;
    }
  }
}
