// Generated code from Butter Knife. Do not modify!
package com.waar.app.fragment.news;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.crystal.crystalpreloaders.widgets.CrystalPreloader;
import com.waar.app.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NewsFragment_ViewBinding<T extends NewsFragment> implements Unbinder {
  protected T target;

  @UiThread
  public NewsFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.swipeRefreshLayout = Utils.findRequiredViewAsType(source, R.id.news_swipe_refresh_layout, "field 'swipeRefreshLayout'", SwipeRefreshLayout.class);
    target.newsList = Utils.findRequiredViewAsType(source, R.id.news_list, "field 'newsList'", RecyclerView.class);
    target.preloader = Utils.findRequiredViewAsType(source, R.id.loader, "field 'preloader'", CrystalPreloader.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.swipeRefreshLayout = null;
    target.newsList = null;
    target.preloader = null;

    this.target = null;
  }
}
