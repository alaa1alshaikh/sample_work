package com.waar.app.dagger.component;

import android.app.Application;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.waar.app.dagger.module.AppModule;
import com.waar.app.dagger.module.AppModule_ProvideApplicationFactory;
import com.waar.app.dagger.module.NetworkModule;
import com.waar.app.dagger.module.NetworkModule_ProvideGsonFactory;
import com.waar.app.dagger.module.NetworkModule_ProvideHttpLoggingInterceptorFactory;
import com.waar.app.dagger.module.NetworkModule_ProvideLanguageTokenFactory;
import com.waar.app.dagger.module.NetworkModule_ProvideOkHttpClientFactory;
import com.waar.app.dagger.module.NetworkModule_ProvideRetrofitFactory;
import com.waar.app.dagger.module.NetworkModule_ProvidesSharedPreferencesFactory;
import com.waar.app.settings.LanguageToken;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerNetworkComponent implements NetworkComponent {
  private Provider<Gson> provideGsonProvider;

  private Provider<HttpLoggingInterceptor> provideHttpLoggingInterceptorProvider;

  private Provider<OkHttpClient> provideOkHttpClientProvider;

  private Provider<Retrofit> provideRetrofitProvider;

  private Provider<Application> provideApplicationProvider;

  private Provider<SharedPreferences> providesSharedPreferencesProvider;

  private Provider<LanguageToken> provideLanguageTokenProvider;

  private DaggerNetworkComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.provideGsonProvider =
        DoubleCheck.provider(NetworkModule_ProvideGsonFactory.create(builder.networkModule));

    this.provideHttpLoggingInterceptorProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideHttpLoggingInterceptorFactory.create(builder.networkModule));

    this.provideOkHttpClientProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideOkHttpClientFactory.create(
                builder.networkModule, provideHttpLoggingInterceptorProvider));

    this.provideRetrofitProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideRetrofitFactory.create(
                builder.networkModule, provideGsonProvider, provideOkHttpClientProvider));

    this.provideApplicationProvider =
        DoubleCheck.provider(AppModule_ProvideApplicationFactory.create(builder.appModule));

    this.providesSharedPreferencesProvider =
        DoubleCheck.provider(
            NetworkModule_ProvidesSharedPreferencesFactory.create(
                builder.networkModule, provideApplicationProvider));

    this.provideLanguageTokenProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideLanguageTokenFactory.create(
                builder.networkModule, providesSharedPreferencesProvider));
  }

  @Override
  public Retrofit retrofit() {
    return provideRetrofitProvider.get();
  }

  @Override
  public LanguageToken languageToken() {
    return provideLanguageTokenProvider.get();
  }

  public static final class Builder {
    private NetworkModule networkModule;

    private AppModule appModule;

    private Builder() {}

    public NetworkComponent build() {
      if (networkModule == null) {
        this.networkModule = new NetworkModule();
      }
      if (appModule == null) {
        throw new IllegalStateException(AppModule.class.getCanonicalName() + " must be set");
      }
      return new DaggerNetworkComponent(this);
    }

    public Builder networkModule(NetworkModule networkModule) {
      this.networkModule = Preconditions.checkNotNull(networkModule);
      return this;
    }

    public Builder appModule(AppModule appModule) {
      this.appModule = Preconditions.checkNotNull(appModule);
      return this;
    }
  }
}
