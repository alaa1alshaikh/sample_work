package com.waar.app.fragment.news;

import com.waar.app.network.news.NewsManger;
import com.waar.app.settings.LanguageToken;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewsPresenter_Factory implements Factory<NewsPresenter> {
  private final Provider<NewsManger> newsMangerProvider;

  private final Provider<LanguageToken> languageTokenProvider;

  public NewsPresenter_Factory(
      Provider<NewsManger> newsMangerProvider, Provider<LanguageToken> languageTokenProvider) {
    assert newsMangerProvider != null;
    this.newsMangerProvider = newsMangerProvider;
    assert languageTokenProvider != null;
    this.languageTokenProvider = languageTokenProvider;
  }

  @Override
  public NewsPresenter get() {
    return new NewsPresenter(newsMangerProvider.get(), languageTokenProvider.get());
  }

  public static Factory<NewsPresenter> create(
      Provider<NewsManger> newsMangerProvider, Provider<LanguageToken> languageTokenProvider) {
    return new NewsPresenter_Factory(newsMangerProvider, languageTokenProvider);
  }
}
