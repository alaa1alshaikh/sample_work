// Generated code from Butter Knife. Do not modify!
package com.waar.app.fragment.zoom;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.waar.app.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ZoomFragment_ViewBinding<T extends ZoomFragment> implements Unbinder {
  protected T target;

  @UiThread
  public ZoomFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.viewPager = Utils.findRequiredViewAsType(source, R.id.photo_pager, "field 'viewPager'", ViewPager.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.viewPager = null;

    this.target = null;
  }
}
