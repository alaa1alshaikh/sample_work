// Generated code from Butter Knife. Do not modify!
package com.waar.app.adapter.slideshow;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.waar.app.R;
import com.waar.app.thirdParty.views.ZoomAbleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewPhotoAdapter_ViewBinding<T extends ViewPhotoAdapter> implements Unbinder {
  protected T target;

  @UiThread
  public ViewPhotoAdapter_ViewBinding(T target, View source) {
    this.target = target;

    target.image = Utils.findRequiredViewAsType(source, R.id.zoom_image, "field 'image'", ZoomAbleImageView.class);
    target.imageNumber = Utils.findRequiredViewAsType(source, R.id.image_number, "field 'imageNumber'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.image = null;
    target.imageNumber = null;

    this.target = null;
  }
}
