package com.waar.app.dagger.module;

import com.waar.app.fragment.news.NewsPresenter;
import com.waar.app.network.news.NewsApi;
import com.waar.app.settings.LanguageToken;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewsModule_ProvideNewsPresenterFactory implements Factory<NewsPresenter> {
  private final NewsModule module;

  private final Provider<NewsApi> newsApiProvider;

  private final Provider<LanguageToken> languageTokenProvider;

  public NewsModule_ProvideNewsPresenterFactory(
      NewsModule module,
      Provider<NewsApi> newsApiProvider,
      Provider<LanguageToken> languageTokenProvider) {
    assert module != null;
    this.module = module;
    assert newsApiProvider != null;
    this.newsApiProvider = newsApiProvider;
    assert languageTokenProvider != null;
    this.languageTokenProvider = languageTokenProvider;
  }

  @Override
  public NewsPresenter get() {
    return Preconditions.checkNotNull(
        module.provideNewsPresenter(newsApiProvider.get(), languageTokenProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<NewsPresenter> create(
      NewsModule module,
      Provider<NewsApi> newsApiProvider,
      Provider<LanguageToken> languageTokenProvider) {
    return new NewsModule_ProvideNewsPresenterFactory(
        module, newsApiProvider, languageTokenProvider);
  }

  /** Proxies {@link NewsModule#provideNewsPresenter(NewsApi, LanguageToken)}. */
  public static NewsPresenter proxyProvideNewsPresenter(
      NewsModule instance, NewsApi newsApi, LanguageToken languageToken) {
    return instance.provideNewsPresenter(newsApi, languageToken);
  }
}
