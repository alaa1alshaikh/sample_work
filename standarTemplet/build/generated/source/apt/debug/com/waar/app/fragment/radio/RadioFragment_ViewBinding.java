// Generated code from Butter Knife. Do not modify!
package com.waar.app.fragment.radio;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.waar.app.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RadioFragment_ViewBinding<T extends RadioFragment> implements Unbinder {
  protected T target;

  @UiThread
  public RadioFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.mPlay = Utils.findRequiredViewAsType(source, R.id.btn_play, "field 'mPlay'", ImageButton.class);
    target.mPause = Utils.findRequiredViewAsType(source, R.id.btn_pause, "field 'mPause'", ImageButton.class);
    target.mStop = Utils.findRequiredViewAsType(source, R.id.btn_stop, "field 'mStop'", ImageButton.class);
    target.seekBar = Utils.findRequiredViewAsType(source, R.id.seekBar, "field 'seekBar'", SeekBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mPlay = null;
    target.mPause = null;
    target.mStop = null;
    target.seekBar = null;

    this.target = null;
  }
}
