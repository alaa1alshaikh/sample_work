package com.waar.app.dagger.module;

import com.waar.app.network.news.NewsApi;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewsModule_ProvideNewsApiFactory implements Factory<NewsApi> {
  private final NewsModule module;

  private final Provider<Retrofit> retrofitProvider;

  public NewsModule_ProvideNewsApiFactory(NewsModule module, Provider<Retrofit> retrofitProvider) {
    assert module != null;
    this.module = module;
    assert retrofitProvider != null;
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public NewsApi get() {
    return Preconditions.checkNotNull(
        module.provideNewsApi(retrofitProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<NewsApi> create(NewsModule module, Provider<Retrofit> retrofitProvider) {
    return new NewsModule_ProvideNewsApiFactory(module, retrofitProvider);
  }

  /** Proxies {@link NewsModule#provideNewsApi(Retrofit)}. */
  public static NewsApi proxyProvideNewsApi(NewsModule instance, Retrofit retrofit) {
    return instance.provideNewsApi(retrofit);
  }
}
