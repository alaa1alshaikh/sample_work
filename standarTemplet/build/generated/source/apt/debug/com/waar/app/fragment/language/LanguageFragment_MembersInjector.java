package com.waar.app.fragment.language;

import com.waar.app.settings.LanguageToken;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class LanguageFragment_MembersInjector implements MembersInjector<LanguageFragment> {
  private final Provider<LanguageToken> languageTokenProvider;

  public LanguageFragment_MembersInjector(Provider<LanguageToken> languageTokenProvider) {
    assert languageTokenProvider != null;
    this.languageTokenProvider = languageTokenProvider;
  }

  public static MembersInjector<LanguageFragment> create(
      Provider<LanguageToken> languageTokenProvider) {
    return new LanguageFragment_MembersInjector(languageTokenProvider);
  }

  @Override
  public void injectMembers(LanguageFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.languageToken = languageTokenProvider.get();
  }

  public static void injectLanguageToken(
      LanguageFragment instance, Provider<LanguageToken> languageTokenProvider) {
    instance.languageToken = languageTokenProvider.get();
  }
}
