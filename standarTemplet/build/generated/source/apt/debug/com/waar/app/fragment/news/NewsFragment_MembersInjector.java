package com.waar.app.fragment.news;

import com.waar.app.settings.LanguageToken;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewsFragment_MembersInjector implements MembersInjector<NewsFragment> {
  private final Provider<Retrofit> mRetrofitProvider;

  private final Provider<LanguageToken> languageTokenProvider;

  public NewsFragment_MembersInjector(
      Provider<Retrofit> mRetrofitProvider, Provider<LanguageToken> languageTokenProvider) {
    assert mRetrofitProvider != null;
    this.mRetrofitProvider = mRetrofitProvider;
    assert languageTokenProvider != null;
    this.languageTokenProvider = languageTokenProvider;
  }

  public static MembersInjector<NewsFragment> create(
      Provider<Retrofit> mRetrofitProvider, Provider<LanguageToken> languageTokenProvider) {
    return new NewsFragment_MembersInjector(mRetrofitProvider, languageTokenProvider);
  }

  @Override
  public void injectMembers(NewsFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.mRetrofit = mRetrofitProvider.get();
    instance.languageToken = languageTokenProvider.get();
  }

  public static void injectMRetrofit(NewsFragment instance, Provider<Retrofit> mRetrofitProvider) {
    instance.mRetrofit = mRetrofitProvider.get();
  }

  public static void injectLanguageToken(
      NewsFragment instance, Provider<LanguageToken> languageTokenProvider) {
    instance.languageToken = languageTokenProvider.get();
  }
}
