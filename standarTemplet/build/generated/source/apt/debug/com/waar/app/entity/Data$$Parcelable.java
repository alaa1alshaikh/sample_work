
package com.waar.app.entity;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.ParcelWrapper;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2016-12-30T22:27+0200")
@SuppressWarnings("unchecked")
public class Data$$Parcelable
    implements Parcelable, ParcelWrapper<com.waar.app.entity.Data>
{

    private com.waar.app.entity.Data data$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Data$$Parcelable.Creator$$0 CREATOR = new Data$$Parcelable.Creator$$0();

    public Data$$Parcelable(android.os.Parcel parcel$$0) {
        com.waar.app.entity.Data data$$2;
        if (parcel$$0 .readInt() == -1) {
            data$$2 = null;
        } else {
            data$$2 = readcom_waar_app_entity_Data(parcel$$0);
        }
        data$$0 = data$$2;
    }

    public Data$$Parcelable(com.waar.app.entity.Data data$$4) {
        data$$0 = data$$4;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$1, int flags) {
        if (data$$0 == null) {
            parcel$$1 .writeInt(-1);
        } else {
            parcel$$1 .writeInt(1);
            writecom_waar_app_entity_Data(data$$0, parcel$$1, flags);
        }
    }

    private com.waar.app.entity.Data readcom_waar_app_entity_Data(android.os.Parcel parcel$$2) {
        com.waar.app.entity.Data data$$1;
        data$$1 = new com.waar.app.entity.Data();
        data$$1 .setPath(parcel$$2 .readString());
        data$$1 .setDescription(parcel$$2 .readString());
        data$$1 .setId(parcel$$2 .readString());
        data$$1 .setVideo(parcel$$2 .readString());
        data$$1 .setTitle(parcel$$2 .readString());
        data$$1 .setType(parcel$$2 .readString());
        return data$$1;
    }

    private void writecom_waar_app_entity_Data(com.waar.app.entity.Data data$$3, android.os.Parcel parcel$$3, int flags$$0) {
        parcel$$3 .writeString(data$$3 .getPath());
        parcel$$3 .writeString(data$$3 .getDescription());
        parcel$$3 .writeString(data$$3 .getId());
        parcel$$3 .writeString(data$$3 .getVideo());
        parcel$$3 .writeString(data$$3 .getTitle());
        parcel$$3 .writeString(data$$3 .getType());
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.waar.app.entity.Data getParcel() {
        return data$$0;
    }

    private final static class Creator$$0
        implements Creator<Data$$Parcelable>
    {


        @Override
        public Data$$Parcelable createFromParcel(android.os.Parcel parcel$$4) {
            return new Data$$Parcelable(parcel$$4);
        }

        @Override
        public Data$$Parcelable[] newArray(int size) {
            return new Data$$Parcelable[size] ;
        }

    }

}
