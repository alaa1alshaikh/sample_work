// Generated code from Butter Knife. Do not modify!
package com.waar.app.adapter.news_adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.waar.app.R;
import com.waar.app.thirdParty.views.CustomTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NewsHolder_ViewBinding<T extends NewsHolder> implements Unbinder {
  protected T target;

  @UiThread
  public NewsHolder_ViewBinding(T target, View source) {
    this.target = target;

    target.personNameAndTime = Utils.findRequiredViewAsType(source, R.id.news_title, "field 'personNameAndTime'", CustomTextView.class);
    target.commentSummery = Utils.findRequiredViewAsType(source, R.id.news_description, "field 'commentSummery'", CustomTextView.class);
    target.personThumb = Utils.findRequiredViewAsType(source, R.id.new_thumb, "field 'personThumb'", ImageView.class);
    target.imageVideo = Utils.findRequiredViewAsType(source, R.id.imageVideo, "field 'imageVideo'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.personNameAndTime = null;
    target.commentSummery = null;
    target.personThumb = null;
    target.imageVideo = null;

    this.target = null;
  }
}
