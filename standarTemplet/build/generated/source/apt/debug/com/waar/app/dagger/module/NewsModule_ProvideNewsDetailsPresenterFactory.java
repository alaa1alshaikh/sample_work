package com.waar.app.dagger.module;

import com.waar.app.fragment.news_deatails.NewsDetailsPresenter;
import com.waar.app.network.news.NewsApi;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewsModule_ProvideNewsDetailsPresenterFactory
    implements Factory<NewsDetailsPresenter> {
  private final NewsModule module;

  private final Provider<NewsApi> newsApiProvider;

  public NewsModule_ProvideNewsDetailsPresenterFactory(
      NewsModule module, Provider<NewsApi> newsApiProvider) {
    assert module != null;
    this.module = module;
    assert newsApiProvider != null;
    this.newsApiProvider = newsApiProvider;
  }

  @Override
  public NewsDetailsPresenter get() {
    return Preconditions.checkNotNull(
        module.provideNewsDetailsPresenter(newsApiProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<NewsDetailsPresenter> create(
      NewsModule module, Provider<NewsApi> newsApiProvider) {
    return new NewsModule_ProvideNewsDetailsPresenterFactory(module, newsApiProvider);
  }

  /** Proxies {@link NewsModule#provideNewsDetailsPresenter(NewsApi)}. */
  public static NewsDetailsPresenter proxyProvideNewsDetailsPresenter(
      NewsModule instance, NewsApi newsApi) {
    return instance.provideNewsDetailsPresenter(newsApi);
  }
}
