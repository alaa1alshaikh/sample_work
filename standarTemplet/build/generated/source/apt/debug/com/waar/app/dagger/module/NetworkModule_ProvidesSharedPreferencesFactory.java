package com.waar.app.dagger.module;

import android.app.Application;
import android.content.SharedPreferences;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvidesSharedPreferencesFactory
    implements Factory<SharedPreferences> {
  private final NetworkModule module;

  private final Provider<Application> applicationProvider;

  public NetworkModule_ProvidesSharedPreferencesFactory(
      NetworkModule module, Provider<Application> applicationProvider) {
    assert module != null;
    this.module = module;
    assert applicationProvider != null;
    this.applicationProvider = applicationProvider;
  }

  @Override
  public SharedPreferences get() {
    return Preconditions.checkNotNull(
        module.providesSharedPreferences(applicationProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<SharedPreferences> create(
      NetworkModule module, Provider<Application> applicationProvider) {
    return new NetworkModule_ProvidesSharedPreferencesFactory(module, applicationProvider);
  }

  /** Proxies {@link NetworkModule#providesSharedPreferences(Application)}. */
  public static SharedPreferences proxyProvidesSharedPreferences(
      NetworkModule instance, Application application) {
    return instance.providesSharedPreferences(application);
  }
}
