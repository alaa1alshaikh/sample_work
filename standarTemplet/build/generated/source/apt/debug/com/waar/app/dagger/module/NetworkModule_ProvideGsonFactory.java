package com.waar.app.dagger.module;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvideGsonFactory implements Factory<Gson> {
  private final NetworkModule module;

  public NetworkModule_ProvideGsonFactory(NetworkModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public Gson get() {
    return Preconditions.checkNotNull(
        module.provideGson(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Gson> create(NetworkModule module) {
    return new NetworkModule_ProvideGsonFactory(module);
  }

  /** Proxies {@link NetworkModule#provideGson()}. */
  public static Gson proxyProvideGson(NetworkModule instance) {
    return instance.provideGson();
  }
}
