package com.waar.app.fragment.news_deatails;

import com.waar.app.network.news.NewsManger;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewsDetailsPresenter_Factory implements Factory<NewsDetailsPresenter> {
  private final Provider<NewsManger> newsMangerProvider;

  public NewsDetailsPresenter_Factory(Provider<NewsManger> newsMangerProvider) {
    assert newsMangerProvider != null;
    this.newsMangerProvider = newsMangerProvider;
  }

  @Override
  public NewsDetailsPresenter get() {
    return new NewsDetailsPresenter(newsMangerProvider.get());
  }

  public static Factory<NewsDetailsPresenter> create(Provider<NewsManger> newsMangerProvider) {
    return new NewsDetailsPresenter_Factory(newsMangerProvider);
  }
}
