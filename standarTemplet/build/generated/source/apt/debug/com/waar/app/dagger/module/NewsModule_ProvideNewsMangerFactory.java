package com.waar.app.dagger.module;

import com.waar.app.network.news.NewsApi;
import com.waar.app.network.news.NewsManger;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewsModule_ProvideNewsMangerFactory implements Factory<NewsManger> {
  private final NewsModule module;

  private final Provider<NewsApi> newsApiProvider;

  public NewsModule_ProvideNewsMangerFactory(NewsModule module, Provider<NewsApi> newsApiProvider) {
    assert module != null;
    this.module = module;
    assert newsApiProvider != null;
    this.newsApiProvider = newsApiProvider;
  }

  @Override
  public NewsManger get() {
    return Preconditions.checkNotNull(
        module.provideNewsManger(newsApiProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<NewsManger> create(NewsModule module, Provider<NewsApi> newsApiProvider) {
    return new NewsModule_ProvideNewsMangerFactory(module, newsApiProvider);
  }

  /** Proxies {@link NewsModule#provideNewsManger(NewsApi)}. */
  public static NewsManger proxyProvideNewsManger(NewsModule instance, NewsApi newsApi) {
    return instance.provideNewsManger(newsApi);
  }
}
