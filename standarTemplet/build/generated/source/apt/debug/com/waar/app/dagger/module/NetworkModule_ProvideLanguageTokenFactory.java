package com.waar.app.dagger.module;

import android.content.SharedPreferences;
import com.waar.app.settings.LanguageToken;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvideLanguageTokenFactory implements Factory<LanguageToken> {
  private final NetworkModule module;

  private final Provider<SharedPreferences> sharedPreferencesProvider;

  public NetworkModule_ProvideLanguageTokenFactory(
      NetworkModule module, Provider<SharedPreferences> sharedPreferencesProvider) {
    assert module != null;
    this.module = module;
    assert sharedPreferencesProvider != null;
    this.sharedPreferencesProvider = sharedPreferencesProvider;
  }

  @Override
  public LanguageToken get() {
    return Preconditions.checkNotNull(
        module.provideLanguageToken(sharedPreferencesProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<LanguageToken> create(
      NetworkModule module, Provider<SharedPreferences> sharedPreferencesProvider) {
    return new NetworkModule_ProvideLanguageTokenFactory(module, sharedPreferencesProvider);
  }

  /** Proxies {@link NetworkModule#provideLanguageToken(SharedPreferences)}. */
  public static LanguageToken proxyProvideLanguageToken(
      NetworkModule instance, SharedPreferences sharedPreferences) {
    return instance.provideLanguageToken(sharedPreferences);
  }
}
