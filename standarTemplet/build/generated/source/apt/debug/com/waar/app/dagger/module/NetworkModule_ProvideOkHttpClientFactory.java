package com.waar.app.dagger.module;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
  private final NetworkModule module;

  private final Provider<HttpLoggingInterceptor> interceptorProvider;

  public NetworkModule_ProvideOkHttpClientFactory(
      NetworkModule module, Provider<HttpLoggingInterceptor> interceptorProvider) {
    assert module != null;
    this.module = module;
    assert interceptorProvider != null;
    this.interceptorProvider = interceptorProvider;
  }

  @Override
  public OkHttpClient get() {
    return Preconditions.checkNotNull(
        module.provideOkHttpClient(interceptorProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<OkHttpClient> create(
      NetworkModule module, Provider<HttpLoggingInterceptor> interceptorProvider) {
    return new NetworkModule_ProvideOkHttpClientFactory(module, interceptorProvider);
  }

  /** Proxies {@link NetworkModule#provideOkHttpClient(HttpLoggingInterceptor)}. */
  public static OkHttpClient proxyProvideOkHttpClient(
      NetworkModule instance, HttpLoggingInterceptor interceptor) {
    return instance.provideOkHttpClient(interceptor);
  }
}
