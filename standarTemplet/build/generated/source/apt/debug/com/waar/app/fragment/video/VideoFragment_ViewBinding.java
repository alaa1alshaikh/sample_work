// Generated code from Butter Knife. Do not modify!
package com.waar.app.fragment.video;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.VideoView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.waar.app.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class VideoFragment_ViewBinding<T extends VideoFragment> implements Unbinder {
  protected T target;

  @UiThread
  public VideoFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.mVideoView = Utils.findRequiredViewAsType(source, R.id.surface_view, "field 'mVideoView'", VideoView.class);
    target.mPlay = Utils.findRequiredViewAsType(source, R.id.play, "field 'mPlay'", ImageButton.class);
    target.mPause = Utils.findRequiredViewAsType(source, R.id.pause, "field 'mPause'", ImageButton.class);
    target.mStop = Utils.findRequiredViewAsType(source, R.id.stop, "field 'mStop'", ImageButton.class);
    target.seekBar = Utils.findRequiredViewAsType(source, R.id.seekBar, "field 'seekBar'", SeekBar.class);
    target.linearLayout = Utils.findRequiredViewAsType(source, R.id.play_lay, "field 'linearLayout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.mVideoView = null;
    target.mPlay = null;
    target.mPause = null;
    target.mStop = null;
    target.seekBar = null;
    target.linearLayout = null;

    this.target = null;
  }
}
