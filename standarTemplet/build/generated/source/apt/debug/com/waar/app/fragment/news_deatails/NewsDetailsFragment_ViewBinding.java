// Generated code from Butter Knife. Do not modify!
package com.waar.app.fragment.news_deatails;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.crystal.crystalpreloaders.widgets.CrystalPreloader;
import com.waar.app.R;
import com.waar.app.thirdParty.views.CustomScrollView;
import com.waar.app.thirdParty.views.CustomTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NewsDetailsFragment_ViewBinding<T extends NewsDetailsFragment> implements Unbinder {
  protected T target;

  @UiThread
  public NewsDetailsFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.imageLayout = Utils.findRequiredViewAsType(source, R.id.pagerLayout, "field 'imageLayout'", RelativeLayout.class);
    target.imageImage = Utils.findRequiredViewAsType(source, R.id.image, "field 'imageImage'", ImageView.class);
    target.videoImage = Utils.findRequiredViewAsType(source, R.id.imageVideo, "field 'videoImage'", ImageView.class);
    target.preloader = Utils.findRequiredViewAsType(source, R.id.loader_news_details, "field 'preloader'", CrystalPreloader.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.advtitle, "field 'title'", CustomTextView.class);
    target.sSummery = Utils.findRequiredViewAsType(source, R.id.advSummery, "field 'sSummery'", CustomTextView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", CustomScrollView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.imageLayout = null;
    target.imageImage = null;
    target.videoImage = null;
    target.preloader = null;
    target.title = null;
    target.sSummery = null;
    target.scrollView = null;

    this.target = null;
  }
}
