package com.waar.app.fragment.language;

import com.waar.app.settings.LanguageToken;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class LanguagePresenter_Factory implements Factory<LanguagePresenter> {
  private final Provider<LanguageToken> languageTokenProvider;

  public LanguagePresenter_Factory(Provider<LanguageToken> languageTokenProvider) {
    assert languageTokenProvider != null;
    this.languageTokenProvider = languageTokenProvider;
  }

  @Override
  public LanguagePresenter get() {
    return new LanguagePresenter(languageTokenProvider.get());
  }

  public static Factory<LanguagePresenter> create(Provider<LanguageToken> languageTokenProvider) {
    return new LanguagePresenter_Factory(languageTokenProvider);
  }
}
