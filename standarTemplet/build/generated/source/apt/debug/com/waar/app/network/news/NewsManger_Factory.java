package com.waar.app.network.news;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NewsManger_Factory implements Factory<NewsManger> {
  private final Provider<NewsApi> newsApiProvider;

  public NewsManger_Factory(Provider<NewsApi> newsApiProvider) {
    assert newsApiProvider != null;
    this.newsApiProvider = newsApiProvider;
  }

  @Override
  public NewsManger get() {
    return new NewsManger(newsApiProvider.get());
  }

  public static Factory<NewsManger> create(Provider<NewsApi> newsApiProvider) {
    return new NewsManger_Factory(newsApiProvider);
  }
}
