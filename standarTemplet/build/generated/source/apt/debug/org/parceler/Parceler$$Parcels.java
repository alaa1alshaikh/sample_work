
package org.parceler;

import java.util.HashMap;
import java.util.Map;
import com.waar.app.entity.Data;
import com.waar.app.entity.Data$$Parcelable;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2016-12-30T22:27+0200")
@SuppressWarnings("unchecked")
public class Parceler$$Parcels
    implements Repository<org.parceler.Parcels.ParcelableFactory>
{

    private final Map<Class, org.parceler.Parcels.ParcelableFactory> map$$0 = new HashMap<Class, org.parceler.Parcels.ParcelableFactory>();

    public Parceler$$Parcels() {
        map$$0 .put(Data.class, new Parceler$$Parcels.Data$$Parcelable$$0());
    }

    public Map<Class, org.parceler.Parcels.ParcelableFactory> get() {
        return map$$0;
    }

    private final static class Data$$Parcelable$$0
        implements org.parceler.Parcels.ParcelableFactory<Data>
    {


        @Override
        public Data$$Parcelable buildParcelable(Data input) {
            return new Data$$Parcelable(input);
        }

    }

}
